package com.avatarqing.alarmclock.util;

public class Constant {
	public static final int REQUEST_ALARM = 0;
	public static final int REQUEST_TIMER = 1;
	public static final int TYPE_ALARM = 1;
	public static final int TYPE_TIMER = 2;

	public static final String FORMAT_TIME = "%1$02d:%2$02d:%3$02d";
	public static final String PREFERENCES = "AlarmClock";
	public static final String PREF_KEY_LAUNCHER_TIMES = "launcher_times";
	public static final String PREF_KEY_IS_RATE = "is_rate";
	public static final String ARG_HOUR = "alarmHour";
	public static final String ARG_MINUTE = "alarmMinute";
	public static final String ARG_SECOND = "alarmSecond";
	public static final String ARG_TIMEPICK_SECOND_ENABLED = "timepickSecondEnabled";
	public static final String ARG_TIMEPICK_MODE = "timepickMode";
	public static final String ARG_DAYSOFWEEK = "alarmDaysOfWeek";
	public static final String ARG_LABEL = "alarmLabel";
	public static final String ARG_RINGTONE = "alarmRingtone";

	public static final String ADMOB_UNIT_ID = "a1526083cf96d77";

	public static final String PAGE_SLIDING_MENU_LEFT = "sliding_menu_left";
	public static final String PAGE_ALARM_LIST = "alarm_list";
	public static final String PAGE_TIMER_LIST = "timer_list";
	public static final String PAGE_SET_ALARM = "set_alarm";
	public static final String PAGE_SET_TIMER = "set_timer";
	public static final String PAGE_SET_SYSTEM_RINGTONE = "set_system_ringtone";
	public static final String PAGE_SET_CUSTOM_RINGTONE = "set_custom_ringtone";
	public static final String PAGE_SET_REPEAT = "set_repeat";
	public static final String PAGE_SET_TIME = "set_time";
	public static final String PAGE_SETTING = "setting";
	public static final String PAGE_ALERT = "alert";
	public static final String PAGE_FULLSCREEN_AD = "fullscreen_ad";

	public static final String EVENT_SET_ALARM = "set_alarm";
	public static final String EVENT_SET_TIMER = "set_timer";
	public static final String EVENT_SNOOZE = "snooze";

	public static final String GA_CATEGORY_ALARM_SET = "alarm_set";
	public static final String GA_ACTION_HOUR_SET = "hour_set";
	public static final String GA_ACTION_REPEAT_SET = "repeat_set";

	public static final String EVENT_PARAM_ALARM_HOUR_PERIOD = "alarm_hour_period";
	public static final String EVENT_PARAM_TIMER_DURATION = "timer_duration";
	public static final String EVENT_PARAM_REPEAT = "repeat_set";
	public static final String EVENT_PARAM_SNOOZE_DURATION = "snooze_duration";

	public static final String TIME_0_1 = "0:00~1:00";
	public static final String TIME_1_2 = "1:00~2:00";
	public static final String TIME_2_3 = "2:00~3:00";
	public static final String TIME_3_4 = "3:00~4:00";
	public static final String TIME_4_5 = "4:00~5:00";
	public static final String TIME_5_6 = "5:00~6:00";
	public static final String TIME_6_7 = "6:00~7:00";
	public static final String TIME_7_8 = "7:00~8:00";
	public static final String TIME_8_9 = "8:00~9:00";
	public static final String TIME_9_10 = "9:00~10:00";
	public static final String TIME_10_11 = "10:00~11:00";
	public static final String TIME_11_12 = "11:00~12:00";
	public static final String TIME_12_13 = "12:00~13:00";
	public static final String TIME_13_14 = "13:00~14:00";
	public static final String TIME_14_15 = "14:00~15:00";
	public static final String TIME_15_16 = "15:00~16:00";
	public static final String TIME_16_17 = "16:00~17:00";
	public static final String TIME_17_18 = "17:00~18:00";
	public static final String TIME_18_19 = "18:00~19:00";
	public static final String TIME_19_20 = "19:00~20:00";
	public static final String TIME_20_21 = "20:00~21:00";
	public static final String TIME_21_22 = "21:00~22:00";
	public static final String TIME_22_23 = "22:00~23:00";
	public static final String TIME_23_24 = "23:00~24:00";

	public static final String REPEAT_ONLY_ONCE = "only_once";
	public static final String REPEAT_EVERYDAY = "everyday";
}
