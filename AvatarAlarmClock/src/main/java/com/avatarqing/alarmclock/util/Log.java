package com.avatarqing.alarmclock.util;

public class Log {
	private static boolean openLog = true;

	public static void openLogging() {
		openLog = true;
	}

	public static void closeLogging() {
		openLog = false;
	}

	public static void i(String tag, String msg) {
		if (openLog) {
			android.util.Log.i(tag, msg);
		}
	}

	public static void e(String tag, String msg) {
		if (openLog) {
			android.util.Log.e(tag, msg);
		}
	}

	public static void w(String tag, String msg) {
		if (openLog) {
			android.util.Log.w(tag, msg);
		}
	}

	public static void d(String tag, String msg) {
		if (openLog) {
			android.util.Log.d(tag, msg);
		}
	}

	public static void v(String tag, String msg) {
		if (openLog) {
			android.util.Log.v(tag, msg);
		}
	}

	public static void printStackTrace(Exception ex) {
		if (openLog) {
			ex.printStackTrace();
		}
	}

	public static void SystemOutPrintln(String str) {
		if (openLog) {
			System.out.println(str);
		}
	}
}
