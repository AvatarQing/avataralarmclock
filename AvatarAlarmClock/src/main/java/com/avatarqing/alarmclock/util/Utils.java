package com.avatarqing.alarmclock.util;

import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager.NameNotFoundException;
import android.net.Uri;
import android.widget.Toast;

public class Utils {
	public static final String TAG = "Utils";

	private static Toast TOAST = null;

	public static void showLongToast(Context context, String text) {
		if (TOAST != null) {
			TOAST.cancel();
		}
		TOAST = Toast.makeText(context, text, Toast.LENGTH_LONG);
		TOAST.show();
	}

	public static void showShortToast(Context context, String text) {
		if (TOAST != null) {
			TOAST.cancel();
		}
		TOAST = Toast.makeText(context, text, Toast.LENGTH_SHORT);
		TOAST.show();
	}

	/** 判断指定包名的应用是否已经安装 */
	public static boolean isAppInstall(Context context, String packageName) {
		boolean installed = false;
		try {
			PackageInfo packageInfo = context.getPackageManager()
					.getPackageInfo(packageName, 0);
			if (packageInfo != null) {
				installed = true;
			}
		} catch (NameNotFoundException e) {
			e.printStackTrace();
		}
		return installed;
	}

	/**
	 * 分享一段文字
	 *
	 * @param context
	 *            上下文环境，启动Activity用
	 * @param subject
	 *            分享文案的主题
	 * @param text
	 *            分享的文字内容
	 * */
	public static void shareText(Context context, String subject, String text) {
		Intent intent = new Intent(Intent.ACTION_SEND);
		intent.setType("text/plain");
		intent.putExtra(Intent.EXTRA_SUBJECT, subject);
		intent.putExtra(Intent.EXTRA_TEXT, text);
		try {
			intent = Intent.createChooser(intent, subject);
			if (intent != null) {
				intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
				context.startActivity(intent);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/** 打开链接 */
	public static void openLink(Context context, String link) {
		Intent intent = new Intent();
		intent.setAction(Intent.ACTION_VIEW);
		intent.setData(Uri.parse(link));
		intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);

		// 判断是否是GooglePlay的链接
		// if (link.startsWith(Const.GOOGLE_PLAY_PREFFIX_HTTP)
		// || link.startsWith(Const.GOOGLE_PLAY_PREFFIX_HTTPS)
		// || link.startsWith(Const.GOOGLE_PLAY_PREFFIX_MARKET)) {
		// // 如果是GooglePlay的链接，再判断是否安装了GooglePlayStore
		// if (isAppInstall(context, Const.GOOGLE_PLAY_PACKAGE_NAME)) {
		// // 用GooglePlayStore打开
		// intent.setPackage(Const.GOOGLE_PLAY_PACKAGE_NAME);
		// }
		// }

		try {
			context.startActivity(intent);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * 调用第三方邮件程序发送文本内容的邮件
	 *
	 * @param context
	 *            上下文环境，用于启动Activity
	 * @param receivers
	 *            邮件接收者，可以是多个
	 * @param subject
	 *            邮件主题
	 * @param content
	 *            邮件的文本内容
	 * */
	public static void sendTextEmail(Context context, String[] receivers,
	                                 String subject, String content) {
		// 将反馈内容发送邮件给作者
		Intent intent = new Intent(Intent.ACTION_SEND);
		intent.setType("message/rfc822");
		// 设置邮件收件人
		intent.putExtra(Intent.EXTRA_EMAIL, receivers);
		// 设置邮件标题
		intent.putExtra(Intent.EXTRA_SUBJECT, subject);
		// 设置邮件内容
		intent.putExtra(Intent.EXTRA_TEXT, content);
		// 调用系统的邮件系统
		intent = Intent.createChooser(intent, subject);
		try {
			if (intent != null) {
				intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
				context.startActivity(intent);
			}
		} catch (Exception e) {
			// 如果设备没有安装可以发送邮件的程序，就会出错，所以手动捕获以下
			e.printStackTrace();
		}
	}

}