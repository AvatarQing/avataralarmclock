package com.avatarqing.alarmclock.util;

import java.util.Calendar;
import java.util.HashSet;
import java.util.Set;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.ContentResolver;
import android.content.ContentUris;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Parcel;
import android.text.TextUtils;
import android.text.format.DateFormat;

import com.avatarqing.alarmclock.application.MyApplication;
import com.avatarqing.alarmclock.entity.Timer;
import com.bounique.tools.alarmclock.R;

/**
 * The Timers provider supplies info about Timer Clock settings
 */
public class TimerHelper {
	private static final String TAG = "TimerHelper";

	// This action triggers the TimerReceiver as well as the TimerKlaxon. It
	// is a public action used in the manifest for receiving Timer broadcasts
	// from the timer manager.
	public static final String TIMER_ALERT_ACTION = "com.joymeng.alarmclock.TIMER_ALERT";

	// A public action sent by TimerKlaxon when the timer has stopped sounding
	// for any reason (e.g. because it has been dismissed from
	// TimerAlertFullScreen,
	// or killed due to an incoming phone call, etc).
	public static final String TIMER_DONE_ACTION = "com.joymeng.alarmclock.TIMER_DONE";

	// TimerAlertFullScreen listens for this broadcast intent, so that other
	// applications
	// can snooze the timer (after TIMER_ALERT_ACTION and before
	// TIMER_DONE_ACTION).
	public static final String TIMER_SNOOZE_ACTION = "com.joymeng.alarmclock.TIMER_SNOOZE";

	// TimerAlertFullScreen listens for this broadcast intent, so that other
	// applications
	// can dismiss the timer (after TIMER_ALERT_ACTION and before
	// TIMER_DONE_ACTION).
	public static final String TIMER_DISMISS_ACTION = "com.joymeng.alarmclock.TIMER_DISMISS";

	// This is a private action used by the TimerKlaxon to update the UI to
	// show the timer has been killed.
	public static final String TIMER_KILLED = "timer_killed";

	// Extra in the TIMER_KILLED intent to indicate to the user how long the
	// timer played before being killed.
	public static final String TIMER_KILLED_TIMEOUT = "timer_killed_timeout";

	// This string is used to indicate a silent timer in the db.
	public static final String TIMER_ALERT_SILENT = "silent";

	// This intent is sent from the notification when the user cancels the
	// snooze alert.
	public static final String CANCEL_DELAY_TIMER = "cancel_delay_timer";

	// This string is used when passing an Timer object through an intent.
	public static final String TIMER_INTENT_EXTRA = "intent.extra.timer";

	// This extra is the raw Timer object data. It is used in the
	// AlarmManagerService to avoid a ClassNotFoundException when filling in
	// the Intent extras.
	public static final String TIMER_RAW_DATA = "intent.extra.timer_raw";

	// This string is used to identify the timer id passed to SetTimer from the
	// list of timers.
	public static final String TIMER_ID = "timer_id";

	public final static String DMS = "kk:mm:ss";

	/**
	 * Creates a new Timer and fills in the given timer's id.
	 *
	 * @return Time when the timer will fire.
	 */
	public static long addTimer(Context context, Timer timer) {
		ContentValues values = createContentValues(timer);
		Uri uri = context.getContentResolver().insert(
				Timer.Columns.CONTENT_URI, values);
		timer.id = (int) ContentUris.parseId(uri);

		long timeInMillis = calculateAlarmTime(timer);
		setNextAlert(context);
		return timeInMillis;
	}

	/**
	 * A convenience method to set an timer in the Timers content provider.
	 *
	 * @return Time when the timer will fire.
	 */
	public static long setTimer(Context context, Timer timer) {
		ContentValues values = createContentValues(timer);
		ContentResolver resolver = context.getContentResolver();
		resolver.update(
				ContentUris.withAppendedId(Timer.Columns.CONTENT_URI, timer.id),
				values, null, null);

		long timeInMillis = calculateAlarmTime(timer);

		setNextAlert(context);

		return timeInMillis;
	}

	/**
	 * Removes an existing Timer. Sets next alert.
	 */
	public static void deleteTimer(Context context, int timerId) {
		if (timerId == Timer.Columns.INVALID_TIMER_ID) {
			return;
		}

		ContentResolver contentResolver = context.getContentResolver();

		Uri uri = ContentUris
				.withAppendedId(Timer.Columns.CONTENT_URI, timerId);
		contentResolver.delete(uri, "", null);

		setNextAlert(context);
	}

	/** 获取已启动的闹钟的数量 */
	public static int countEnabledTimers(Context context) {
		Cursor cur = getFilteredTimersCursor(context.getContentResolver());
		int count = 0;
		if (cur != null) {
			count = cur.getCount();
			cur.close();
		}
		return count;
	}

	/**
	 * Queries all timers
	 *
	 * @return cursor over all timers
	 */
	public static Cursor getTimersCursor(ContentResolver contentResolver) {
		return contentResolver.query(Timer.Columns.CONTENT_URI,
				Timer.Columns.TIMER_QUERY_COLUMNS, null, null,
				Timer.Columns.DEFAULT_SORT_ORDER);
	}

	/** 获取所有已启动的闹钟 */
	private static Cursor getFilteredTimersCursor(
			ContentResolver contentResolver) {
		return contentResolver.query(Timer.Columns.CONTENT_URI,
				Timer.Columns.TIMER_QUERY_COLUMNS, Timer.Columns.WHERE_ENABLED,
				null, Timer.Columns.DEFAULT_SORT_ORDER);
	}

	/**
	 * Return an Timer object representing the timer id in the database. Returns
	 * null if no timer exists.
	 */
	public static Timer getTimer(ContentResolver contentResolver, int timerId) {
		Cursor cursor = contentResolver.query(
				ContentUris.withAppendedId(Timer.Columns.CONTENT_URI, timerId),
				Timer.Columns.TIMER_QUERY_COLUMNS, null, null, null);
		Timer timer = null;
		if (cursor != null) {
			if (cursor.moveToFirst()) {
				timer = new Timer(cursor);
			}
			cursor.close();
		}
		return timer;
	}

	/**
	 * A convenience method to enable or disable an timer.
	 *
	 * @param id
	 *            corresponds to the _id column
	 * @param enabled
	 *            corresponds to the ENABLED column
	 */

	public static void enableTimer(final Context context, final int id,
	                               boolean enabled) {
		Timer timer = getTimer(context.getContentResolver(), id);
		enableTimerInternal(context, timer, enabled);
		setNextAlert(context);
	}

	private static void enableTimerInternal(final Context context,
	                                        final Timer timer, boolean enabled) {
		if (timer == null) {
			return;
		}
		ContentResolver resolver = context.getContentResolver();

		ContentValues values = new ContentValues(2);
		values.put(Timer.Columns.ENABLED, enabled ? 1 : 0);

		// If we are enabling the timer, calculate timer time since the time
		// value in Timer may be old.
		if (enabled) {
			values.put(Timer.Columns.START_TIME, System.currentTimeMillis());
		} else {
			values.put(Timer.Columns.START_TIME, 0);
		}

		resolver.update(
				ContentUris.withAppendedId(Timer.Columns.CONTENT_URI, timer.id),
				values, null, null);

		Log.v(TAG, "enableTimerInternal()--->timer[" + timer.id + "]"
				+ timer.label + ",enable:" + enabled);
	}

	/**
	 * Disables non-repeating timers that have passed. Called at boot.
	 */
	public static void disableExpiredTimers(final Context context) {
		Cursor cur = getFilteredTimersCursor(context.getContentResolver());
		long now = System.currentTimeMillis();
		Timer timer = null;
		long remainTime = 0;
		try {
			if (cur.moveToFirst()) {
				do {
					timer = new Timer(cur);
					remainTime = calculateRemainTime(timer);

					if (remainTime < now) {
						Log.v(TAG,
								"Disabling expired timer set for "
										+ formatTime(context, timer.hour,
										timer.minutes, timer.second,
										timer.startTime));
						enableTimerInternal(context, timer, false);
					}
				} while (cur.moveToNext());
			}
		} finally {
			cur.close();
		}
	}

	/**
	 * Sets alert in TimerManger and StatusBar. This is what will actually
	 * launch the alert when the timer triggers.
	 */
	private static void enableAlert(Context context, final Timer timer,
	                                final long atTimeInMillis) {

		AlarmManager am = (AlarmManager) context
				.getSystemService(Context.ALARM_SERVICE);

		Intent intent = new Intent(TIMER_ALERT_ACTION);

		// XXX: This is a slight hack to avoid an exception in the remote
		// AlarmManagerService process. The AlarmManager adds extra data to
		// this Intent which causes it to inflate. Since the remote process
		// does not know about the Timer class, it throws a
		// ClassNotFoundException.
		//
		// To avoid this, we marshall the data ourselves and then parcel a plain
		// byte[] array. The TimerReceiver class knows to build the Timer
		// object from the byte[] array.
		Parcel out = Parcel.obtain();
		timer.writeToParcel(out, 0);
		out.setDataPosition(0);
		intent.putExtra(TIMER_RAW_DATA, out.marshall());

		PendingIntent sender = PendingIntent.getBroadcast(context,
				Constant.REQUEST_TIMER, intent,
				PendingIntent.FLAG_CANCEL_CURRENT);
		am.set(AlarmManager.RTC_WAKEUP, atTimeInMillis, sender);

		// 是不是要设置最近的一个闹钟到系统设置
		Calendar c = Calendar.getInstance();
		c.setTimeInMillis(atTimeInMillis);
		String timeString = formatTime(context, c);

		Log.v(TAG, "enableAlert()--->timer[" + timer.id + "]" + timer.label
				+ " atTime:" + timeString);
	}

	/**
	 * Disables alert in TimerManger and StatusBar.
	 *
	 * @param id
	 *            Timer ID.
	 */
	private static void disableAlert(Context context) {
		Log.v(TAG, "disableAlert()");
		AlarmManager am = (AlarmManager) context
				.getSystemService(Context.ALARM_SERVICE);
		PendingIntent sender = PendingIntent.getBroadcast(context,
				Constant.REQUEST_TIMER, new Intent(TIMER_ALERT_ACTION),
				PendingIntent.FLAG_CANCEL_CURRENT);
		am.cancel(sender);
	}

	private static Timer calculateNextAlert(final Context context) {
		Log.v(TAG, "calculateNextAlert()-------->start");
		long minTime = Long.MAX_VALUE;
		long remainTime = 0;
		Set<Timer> timers = new HashSet<Timer>();

		final Cursor cursor = getFilteredTimersCursor(context
				.getContentResolver());
		Log.v(TAG,
				"calculateNextAlert() avaliable timer count:"
						+ cursor.getCount());
		if (cursor != null) {
			try {
				if (cursor.moveToFirst()) {
					do {
						final Timer a = new Timer(cursor);
						timers.add(a);
					} while (cursor.moveToNext());
				}
			} finally {
				cursor.close();
			}
		}

		Timer timer = null;

		for (Timer a : timers) {
			remainTime = calculateRemainTime(a);

			if (remainTime <= 0) {
				Log.v(TAG,
						"Disabling expired timer ["
								+ a.id
								+ "]"
								+ a.label
								+ " set for "
								+ formatTime(context, a.hour, a.minutes,
								a.second, a.startTime));
				// Expired timer, disable it and move along.
				enableTimerInternal(context, a, false);
				continue;
			}
			if (remainTime < minTime) {
				minTime = remainTime;
				timer = a;
			}
		}
		Log.v(TAG, "calculateNextAlert()-------->end");
		return timer;
	}

	/**
	 * Called at system startup, on time/timezone change, and whenever the user
	 * changes timer settings. loads all timers, activates next alert.
	 */
	public static void setNextAlert(final Context context) {
		Log.v(TAG, "setNextAlert()--->start");
		final Timer timer = calculateNextAlert(context);
		if (timer != null) {
			long alarmTime = calculateAlarmTime(timer);
			enableAlert(context, timer, alarmTime);
		} else {
			disableAlert(context);
		}
		Log.v(TAG, "setNextAlert()--->end");
	}

	private static ContentValues createContentValues(Timer timer) {
		ContentValues values = new ContentValues(8);

		values.put(Timer.Columns.ENABLED, timer.enabled ? 1 : 0);
		values.put(Timer.Columns.HOUR, timer.hour);
		values.put(Timer.Columns.MINUTES, timer.minutes);
		values.put(Timer.Columns.SECOND, timer.second);
		values.put(Timer.Columns.START_TIME,
				timer.enabled ? System.currentTimeMillis() : 0);
		values.put(Timer.Columns.VIBRATE, timer.vibrate ? 1 : 0);

		if (timer.label == null || TextUtils.isEmpty(timer.label)) {
			timer.label = MyApplication.CONTEXT.getString(R.string.timer);
		}

		values.put(Timer.Columns.MESSAGE, timer.label);

		// A null alert Uri indicates a silent timer.
		values.put(
				Timer.Columns.ALERT,
				timer.alert == null ? TIMER_ALERT_SILENT : timer.alert
						.toString());

		return values;
	}

	/** 计算给定的定时器的剩余响铃时间 */
	public static long calculateRemainTime(Timer timer) {
		return calculateRemainTime(timer.hour, timer.minutes, timer.second,
				timer.startTime);
	}

	/** 计算给定的定时器的剩余响铃时间 */
	public static long calculateRemainTime(int hour, int minute, int second,
	                                       long startTime) {
		long remainTime = 0;
		int delta = hour * 60 * 60 * 1000 + minute * 60 * 1000 + second * 1000;
		long destTime = startTime + delta;
		long now = System.currentTimeMillis();
		if (now > destTime) {
			remainTime = 0;
		} else {
			remainTime = destTime - now;
		}
		return remainTime;
	}

	/** 计算定时器的响铃时刻的毫秒数 */
	public static long calculateAlarmTime(Timer timer) {
		return calculateAlarmTime(timer.hour, timer.minutes, timer.second,
				timer.startTime).getTimeInMillis();
	}

	/** 计算定时器的响铃时刻 */
	public static Calendar calculateAlarmTime(int hour, int minute, int second,
	                                          long startTime) {
		int delta = hour * 60 * 60 * 1000 + minute * 60 * 1000 + second * 1000;
		long destTime = startTime + delta;
		Calendar c = Calendar.getInstance();
		c.setTimeInMillis(destTime);
		return c;
	}

	public static String formatTime(final Context context, Timer timer) {
		return formatTime(context, timer.hour, timer.minutes, timer.second,
				timer.startTime);
	}

	public static String formatTime(final Context context, int hour,
	                                int minute, int second, long startTime) {
		Calendar c = calculateAlarmTime(hour, minute, second, startTime);
		return formatTime(context, c);
	}

	/* used by TimerAlert */
	public static String formatTime(final Context context, Calendar c) {
		return (c == null) ? "" : (String) DateFormat.format(DMS, c);
	}

}