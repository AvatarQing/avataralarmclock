package com.avatarqing.alarmclock.util;

import java.util.HashSet;
import java.util.Set;

import android.content.SharedPreferences;

public class SharedPreferencesProxy {

	private final static String regularEx = "&";

	public static Set<String> getStringSet(SharedPreferences prefs, String key,
			Set<String> defValues) {
		String str = prefs.getString(key, null);
		Set<String> result = defValues;
		if (str != null) {
			String[] values = str.split(regularEx);
			result = new HashSet<String>();
			for (String value : values) {
				if (!value.isEmpty()) {
					result.add(value);
				}
			}
			Log.d("SharedPreferencesProxy", "getStringSet()--->" + str);
		}
		return result;
	}

	public static SharedPreferences.Editor putStringSet(
			SharedPreferences.Editor ed, String key, Set<String> values) {
		String str = "";
		if (values != null | !values.isEmpty()) {
			Object[] objects = values.toArray();
			for (Object obj : objects) {
				str += obj.toString();
				str += regularEx;
			}
			ed.putString(key, str);
		}
		return ed;
	}
}
