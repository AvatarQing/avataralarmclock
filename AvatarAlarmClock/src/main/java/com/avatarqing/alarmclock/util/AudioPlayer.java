package com.avatarqing.alarmclock.util;

import android.content.Context;
import android.content.Intent;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.media.MediaPlayer.OnCompletionListener;
import android.media.MediaPlayer.OnErrorListener;
import android.media.MediaPlayer.OnPreparedListener;
import android.net.Uri;
import android.os.PowerManager;
import android.support.v4.content.LocalBroadcastManager;

import com.avatarqing.alarmclock.application.MyApplication;

public class AudioPlayer implements OnCompletionListener, OnPreparedListener,
		OnErrorListener {

	/** 广播ACTION名称的前缀 */
	private static final String TAG = AudioPlayer.class.getSimpleName();

	/** 一个广播ACTION，歌曲开始播放时发送此广播 */
	public static final String ACTION_PLAY = AudioPlayer.class.getName()
			+ ".ACTION_PLAY";

	/** 一个广播ACTION，歌曲暂停播放时发送此广播 */
	public static final String ACTION_PAUSE = AudioPlayer.class.getName()
			+ ".ACTION_PAUSE";

	/** 一个广播ACTION，歌曲停止播放发送此广播 */
	public static final String ACTION_STOP = AudioPlayer.class.getName()
			+ ".ACTION_STOP";

	/**
	 * 歌曲播放的状态(Preparing,Playing,Paused,Stopped)
	 */
	public static final int STATE_STOPPED = 0; // MediaPlayer已经停止工作，不再准备播放
	public static final int STATE_PREPARING = 1; // MediaPlayer正在准备中
	public static final int STATE_PLAYING = 2; // 正在播放（MediaPlayer已经准备好了）
	// （但是当丢失音频焦点时，MediaPlayer在此状态下实际上也许已经暂停了，
	// 但是我们仍然保持这个状态，以表明我们必须在一获得音频焦点时就返回播放状态）
	public static final int STATE_PAUSED = 3; // 播放暂停 (MediaPlayer处于准备好了的状态)

	private int mState = STATE_STOPPED;

	/** 丢失音频焦点，我们为媒体播放设置一个低音量(1.0f为最大)，而不是停止播放 */
	private static final float DUCK_VOLUME = 0.1f;

	private Context mContext = null;
	private AudioFocusHelper mAudioFocusHelper = null;

	private Uri mPlayingUri = null;

	private MediaPlayer mMediaPlayer = null;

	private LocalBroadcastManager mLocalBroadcastManager = null;

	private AudioPlayer() {
		mContext = MyApplication.CONTEXT;
		mAudioFocusHelper = new AudioFocusHelper(mContext, mMusicFocusable);
		mLocalBroadcastManager = LocalBroadcastManager.getInstance(mContext);
	}

	private static AudioPlayer mController = null;

	public static AudioPlayer getInstance() {
		if (mController == null) {
			mController = new AudioPlayer();
		}
		return mController;
	}

	public void release() {
		processStopRequest(true);
	}

	/**
	 * 返回当前MediaPlayer的状态
	 *
	 * @return 当前MediaPlayer的状态
	 */
	public int getState() {
		return mState;
	}

	public int getPlayingProgress() {
		if (mMediaPlayer != null && mMediaPlayer.isPlaying()) {
			return mMediaPlayer.getCurrentPosition();
		}
		return 0;
	}

	public int getDuration() {
		if (mMediaPlayer != null && mState != STATE_STOPPED) {
			return mMediaPlayer.getDuration();
		}
		return 0;
	}

	public void seekTo(int newPosition) {
		if (mMediaPlayer != null
				&& (mState == STATE_PLAYING || mState == STATE_PAUSED)) {
			mMediaPlayer.seekTo(newPosition);
		}
	}

	public Uri getPlayingUri() {
		return mPlayingUri;
	}

	public void processPlayRequest(String filePath) {
		processPlayRequest(Uri.parse(filePath));
	}

	public void processPlayRequest(Uri uri) {
		Log.i(TAG, "processPlayRequest");
		mAudioFocusHelper.tryToGetAudioFocus();
		if (mState == STATE_PAUSED) {
			configAndStartMediaPlayer();
			mState = STATE_PLAYING;
		} else if (mState != STATE_PREPARING) {
			playSong(uri);
			mPlayingUri = uri;
		}
	}

	public void processPauseRequest() {
		Log.i(TAG, "processPauseRequest");
		if (mState == STATE_PLAYING) {
			mState = STATE_PAUSED;
			mMediaPlayer.pause();
			mLocalBroadcastManager.sendBroadcast(new Intent(ACTION_PAUSE));
		}
	}

	public void processStopRequest() {
		processStopRequest(false);
	}

	public void processStopRequest(boolean force) {
		Log.i(TAG, "processStopRequest");
		if (mState != STATE_STOPPED || force) {
			mState = STATE_STOPPED;
			// 释放�?��持有的资�?
			relaxResources();
			mAudioFocusHelper.giveUpAudioFocus();
			mLocalBroadcastManager.sendBroadcast(new Intent(ACTION_STOP));
			mPlayingUri = null;
		}
	}

	private void playSong(Uri uri) {
		Log.i(TAG, "playSong");
		if (uri == null) {
			return;
		}

		// File file = new File(path);
		// if (!file.exists() && !file.isDirectory()) {
		// Log.i(TAG, "source file does not existed");
		// processStopRequest(true);
		// return;
		// }
		mState = STATE_STOPPED;
		try {
			createMediaPlayerIfNeeded();
			mMediaPlayer.setAudioStreamType(AudioManager.STREAM_MUSIC);

			mMediaPlayer.setDataSource(MyApplication.CONTEXT, uri);

			mState = STATE_PREPARING;

			// 在后台准备MediaPlayer，准备完成后会调用OnPreparedListener的onPrepared()方法�?
			// 在MediaPlayer准备好之前，我们不能调用其start()方法
			mMediaPlayer.prepareAsync();

		} catch (Exception ex) {
			Log.e("MusicService",
					"IOException playing next song: " + ex.getMessage());
			ex.printStackTrace();
		}
	}

	/**
	 * 释放本服务所使用的资源，
	 */
	private void relaxResources() {
		// 停止并释放MediaPlayer
		if (mMediaPlayer != null) {
			mMediaPlayer.reset();
			mMediaPlayer.release();
			mMediaPlayer = null;
		}
	}

	/**
	 * 确保MediaPlayer存在，并且已经被重置。 这个方法将会在需要时创建一个MediaPlayer，
	 * 或者重置一个已存在的MediaPlayer。
	 */
	private void createMediaPlayerIfNeeded() {
		if (mMediaPlayer == null) {
			mMediaPlayer = new MediaPlayer();

			// 确保我们的MediaPlayer在播放时获取了一个唤醒锁，
			// 如果不这样做，当歌曲播放很久时，CPU进入休眠从而导致播放停止
			// 要使用唤醒锁，要确保在AndroidManifest.xml中声明了android.permission.WAKE_LOCK权限
			mMediaPlayer.setWakeMode(mContext, PowerManager.PARTIAL_WAKE_LOCK);

			// 在MediaPlayer在它准备完成时、完成播放时、发生错误时通过监听器通知我们，
			// 以便我们做出相应处理
			mMediaPlayer.setOnPreparedListener(this);
			mMediaPlayer.setOnCompletionListener(this);
			mMediaPlayer.setOnErrorListener(this);
		} else {
			mMediaPlayer.reset();
		}
	}

	/**
	 * 根据音频焦点的设置重新设置MediaPlayer的参数，然后启动或者重启它。 如果我们拥有音频焦点，则正常播放;
	 * 如果没有音频焦点，根据当前的焦点设置将MediaPlayer切换为“暂停”状态或者低声播放。
	 * 这个方法已经假设mPlayer不为空，所以如果要调用此方法，确保正确的使用它。
	 */
	private void configAndStartMediaPlayer() {
		if (mAudioFocusHelper.getAudioFocus() == AudioFocusHelper.NoFocusNoDuck) {
			// 如果丢失了音频焦点也不允许低声播放，我们必须让播放暂停，即使mState处于Playing状态。
			// 但是我们并不修改mState的状态，因为我们会在获得音频焦点时返回立即返回播放状态。
			if (mMediaPlayer.isPlaying()) {
				mMediaPlayer.pause();
				mLocalBroadcastManager.sendBroadcast(new Intent(ACTION_PAUSE));
			}
			return;
		} else if (mAudioFocusHelper.getAudioFocus() == AudioFocusHelper.NoFocusCanDuck)
			// 设置一个较为安静的音量
			mMediaPlayer.setVolume(DUCK_VOLUME, DUCK_VOLUME);
		else {
			// 设置大声播放
			mMediaPlayer.setVolume(1.0f, 1.0f);
		}
		if (!mMediaPlayer.isPlaying()) {
			mMediaPlayer.start();
			mLocalBroadcastManager.sendBroadcast(new Intent(ACTION_PLAY));
		}
	}

	@Override
	public final boolean onError(MediaPlayer mp, int what, int extra) {
		switch (what) {
			case MediaPlayer.MEDIA_ERROR_NOT_VALID_FOR_PROGRESSIVE_PLAYBACK:
				Log.e(TAG, "Error: "
						+ "MEDIA_ERROR_NOT_VALID_FOR_PROGRESSIVE_PLAYBACK"
						+ ", extra=" + String.valueOf(extra));
				break;
			case MediaPlayer.MEDIA_ERROR_SERVER_DIED:
				Log.e(TAG, "Error: " + "MEDIA_ERROR_SERVER_DIED" + ", extra="
						+ String.valueOf(extra));
				break;
			case MediaPlayer.MEDIA_ERROR_UNKNOWN:
				Log.e(TAG,
						"Error: " + "MEDIA_ERROR_UNKNOWN" + ", extra="
								+ String.valueOf(extra));
				break;
			case -38:
				Log.e(TAG, "Error: what" + what + ", extra=" + extra);
				break;
			default:
				Log.e(TAG, "Error: what" + what + ", extra=" + extra);
				break;
		}

		processStopRequest(true);
		mPlayingUri = null;
		return true; // true表示我们处理了发生的错误
	}

	@Override
	public final void onPrepared(MediaPlayer mp) {
		// 准备完成了，可以播放歌曲了
		mState = STATE_PLAYING;
		configAndStartMediaPlayer();

		mLocalBroadcastManager.sendBroadcast(new Intent(ACTION_PLAY));
	}

	@Override
	public final void onCompletion(MediaPlayer mp) {
		processStopRequest(true);
		mPlayingUri = null;
	}

	private MusicFocusable mMusicFocusable = new MusicFocusable() {

		@Override
		public final void onGainedAudioFocus() {
			Log.i(TAG, "gained audio focus.");

			// 用新的音频焦点状态来重置MediaPlayer
			if (mState == STATE_PLAYING) {
				configAndStartMediaPlayer();
			}
		}

		@Override
		public final void onLostAudioFocus() {
			Log.i(TAG, "lost audio focus.");

			// 以新的焦点参数启动/重启/暂停MediaPlayer
			if (mMediaPlayer != null && mMediaPlayer.isPlaying()) {
				configAndStartMediaPlayer();
			}
		}
	};

	public interface MusicFocusable {
		/** Signals that audio focus was gained. */
		public void onGainedAudioFocus();

		/** Signals that audio focus was lost. */
		public void onLostAudioFocus();
	}

	/**
	 * Convenience class to deal with audio focus. This class deals with
	 * everything related to audio focus: it can request and abandon focus, and
	 * will intercept focus change events and deliver them to a MusicFocusable
	 * interface .
	 * <p/>
	 * This class can only be used on SDK level 8 and above, since it uses API
	 * features that are not available on previous SDK's.
	 */
	class AudioFocusHelper implements AudioManager.OnAudioFocusChangeListener {
		/**
		 * Represents something that can react to audio focus events. We
		 * implement this instead of just using
		 * AudioManager.OnAudioFocusChangeListener because that interface is
		 * only available in SDK level 8 and above, and we want our application
		 * to work on previous SDKs.
		 */

		AudioManager mAM;
		MusicFocusable mFocusable;

		// do we have audio focus?
		public static final int NoFocusNoDuck = 0; // we don't have audio focus,
		// and
		// can't duck
		public static final int NoFocusCanDuck = 1; // we don't have focus, but
		// can
		// play at a low volume
		// ("ducking")
		public static final int Focused = 2; // we have full audio focus

		private int mAudioFocus = NoFocusNoDuck;

		public AudioFocusHelper(Context ctx, MusicFocusable focusable) {
			if (android.os.Build.VERSION.SDK_INT >= 8) {
				mAM = (AudioManager) ctx
						.getSystemService(Context.AUDIO_SERVICE);
				mFocusable = focusable;
			} else {
				mAudioFocus = Focused; // no focus feature, so we always "have"
				// audio focus
			}
		}

		/** Requests audio focus. Returns whether request was successful or not. */
		public boolean requestFocus() {
			return AudioManager.AUDIOFOCUS_REQUEST_GRANTED == mAM
					.requestAudioFocus(this, AudioManager.STREAM_MUSIC,
							AudioManager.AUDIOFOCUS_GAIN);
		}

		/** Abandons audio focus. Returns whether request was successful or not. */
		public boolean abandonFocus() {
			return AudioManager.AUDIOFOCUS_REQUEST_GRANTED == mAM
					.abandonAudioFocus(this);
		}

		public void giveUpAudioFocus() {
			if (mAudioFocus == Focused && android.os.Build.VERSION.SDK_INT >= 8
					&& abandonFocus())
				mAudioFocus = NoFocusNoDuck;
		}

		public void tryToGetAudioFocus() {
			if (mAudioFocus != Focused && android.os.Build.VERSION.SDK_INT >= 8
					&& requestFocus())
				mAudioFocus = Focused;
		}

		/**
		 * Called by AudioManager on audio focus changes. We implement this by
		 * calling our MusicFocusable appropriately to relay the message.
		 */
		public void onAudioFocusChange(int focusChange) {
			if (mFocusable == null)
				return;
			switch (focusChange) {
				case AudioManager.AUDIOFOCUS_GAIN:
					mAudioFocus = Focused;
					mFocusable.onGainedAudioFocus();
					break;
				case AudioManager.AUDIOFOCUS_LOSS:
				case AudioManager.AUDIOFOCUS_LOSS_TRANSIENT:
					mAudioFocus = NoFocusNoDuck;
					mFocusable.onLostAudioFocus();
					break;
				case AudioManager.AUDIOFOCUS_LOSS_TRANSIENT_CAN_DUCK:
					mAudioFocus = NoFocusCanDuck;
					mFocusable.onLostAudioFocus();
					break;
				default:
			}
		}

		public int getAudioFocus() {
			return mAudioFocus;
		}
	}
}
