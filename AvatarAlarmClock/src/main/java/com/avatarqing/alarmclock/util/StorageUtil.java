package com.avatarqing.alarmclock.util;

import android.os.Environment;

public class StorageUtil {

	public static boolean isExternalStorageAvaliable() {
		return Environment.getExternalStorageState().equals(
				Environment.MEDIA_MOUNTED);
	}
}
