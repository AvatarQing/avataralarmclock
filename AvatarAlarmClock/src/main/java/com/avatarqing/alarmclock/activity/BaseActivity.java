package com.avatarqing.alarmclock.activity;

import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;

import com.boutique.lib.analytics.ActivityLifecycleAction;

public class BaseActivity extends ActionBarActivity {

	private ActivityLifecycleAction action = new ActivityLifecycleAction();

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		action.onCreate(this);
	}

	@Override
	protected void onStart() {
		super.onStart();
		action.onStart(this);
	}

	@Override
	protected void onResume() {
		super.onResume();
		action.onResume(this);
	}

	@Override
	protected void onPause() {
		super.onPause();
		action.onPause(this);
	}

	@Override
	protected void onStop() {
		super.onStop();
		action.onStop(this);
	}
}
