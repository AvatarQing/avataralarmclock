package com.avatarqing.alarmclock.activity;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.app.NotificationCompat;
import android.view.KeyEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.TextView;

import com.avatarqing.alarmclock.entity.Alarm;
import com.avatarqing.alarmclock.entity.Timer;
import com.avatarqing.alarmclock.receiver.AlarmReceiver;
import com.avatarqing.alarmclock.util.AlarmHelper;
import com.avatarqing.alarmclock.util.Constant;
import com.avatarqing.alarmclock.util.Log;
import com.avatarqing.alarmclock.util.TimerHelper;
import com.avatarqing.alarmclock.util.ToastUtil;
import com.avatarqing.alarmclock.widget.SlideButtonBar;
import com.avatarqing.alarmclock.widget.SlideButtonBar.Callback;
import com.bounique.tools.alarmclock.R;
import com.umeng.analytics.MobclickAgent;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;

/**
 * Alarm Clock alarm alert: pops visible indicator and plays alarm tone. This
 * activity is the full screen version which shows over the lock screen with the
 * wallpaper as the background.
 */
public class AlarmAlertFullScreen extends BaseActivity {
	private static final String TAG = "AlarmAlertFullScreen";

	private static final int VOLUME_BEHAVIOR_SNOOZE = 1;
	private static final int VOLUME_BEHAVIOR_DISMISS = 2;

	// These defaults must match the values in res/xml/settings.xml
	private static final String DEFAULT_SNOOZE = "10";
	private static final String DEFAULT_VOLUME_BEHAVIOR = VOLUME_BEHAVIOR_DISMISS
			+ "";
	protected static final String SCREEN_OFF = "screen_off";

	private Alarm mAlarm = null;
	private Timer mTimer = null;
	private int mVolumeBehavior;

	private ViewGroup mContainer = null;
	private TextView mTvLabel = null;
	private TextView mTvDate = null;
	private Button mBtnSnooze = null;
	private SlideButtonBar mBtnDismiss = null;

	private int mCurrentType = -1;
	// Receives the ALARM_KILLED action from the AlarmKlaxon,
	// and also ALARM_SNOOZE_ACTION / ALARM_DISMISS_ACTION from other
	// applications
	private BroadcastReceiver mReceiver = new BroadcastReceiver() {
		@Override
		public void onReceive(Context context, Intent intent) {
			String action = intent.getAction();
			if (action.equals(AlarmHelper.ALARM_SNOOZE_ACTION)) {
				snooze();
			} else if (action.equals(AlarmHelper.ALARM_DISMISS_ACTION)) {
				dismiss(false);
			} else if (action.equals(AlarmHelper.ALARM_KILLED)) {
				Alarm alarm = intent
						.getParcelableExtra(AlarmHelper.ALARM_INTENT_EXTRA);
				Log.d(TAG, "ALARM_KILLED---> mAlarm:" + mAlarm);
				if (alarm != null && mAlarm != null && mAlarm.id == alarm.id) {
					dismiss(true);
				}
			} else if (action.equals(TimerHelper.TIMER_SNOOZE_ACTION)) {
			} else if (action.equals(TimerHelper.TIMER_DISMISS_ACTION)) {
				dismiss(false);
			} else if (action.equals(TimerHelper.TIMER_KILLED)) {
				Timer timer = intent
						.getParcelableExtra(TimerHelper.TIMER_INTENT_EXTRA);
				if (timer != null && mTimer != null && mTimer.id == timer.id) {
					dismiss(true);
				}
			}
		}
	};

	@Override
	public void onCreate(Bundle icicle) {
		Log.d(TAG, "onCreate");
		super.onCreate(icicle);
		initAnalytics();

		if (getIntent().hasExtra(AlarmHelper.ALARM_INTENT_EXTRA)) {
			mCurrentType = Constant.TYPE_ALARM;
			mAlarm = getIntent().getParcelableExtra(
					AlarmHelper.ALARM_INTENT_EXTRA);
		} else if (getIntent().hasExtra(TimerHelper.TIMER_INTENT_EXTRA)) {
			mCurrentType = Constant.TYPE_TIMER;
			mTimer = getIntent().getParcelableExtra(
					TimerHelper.TIMER_INTENT_EXTRA);
		}

		// Get the volume/camera button behavior setting
		final String vol = PreferenceManager.getDefaultSharedPreferences(this)
				.getString(SettingsActivity.KEY_VOLUME_BEHAVIOR,
						DEFAULT_VOLUME_BEHAVIOR);
		mVolumeBehavior = Integer.parseInt(vol);

		final Window win = getWindow();
		win.addFlags(WindowManager.LayoutParams.FLAG_SHOW_WHEN_LOCKED
				| WindowManager.LayoutParams.FLAG_DISMISS_KEYGUARD);

		// Turn on the screen unless we are being launched from the AlarmAlert
		// subclass as a result of the screen turning off.
		if (!getIntent().getBooleanExtra(SCREEN_OFF, false)) {
			win.addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON
					| WindowManager.LayoutParams.FLAG_TURN_SCREEN_ON
					| WindowManager.LayoutParams.FLAG_ALLOW_LOCK_WHILE_SCREEN_ON);
		}

		updateLayout();

		// Register to get the alarm killed/snooze/dismiss intent.
		IntentFilter filter = new IntentFilter();
		filter.addAction(AlarmHelper.ALARM_KILLED);
		filter.addAction(AlarmHelper.ALARM_SNOOZE_ACTION);
		filter.addAction(AlarmHelper.ALARM_DISMISS_ACTION);
		filter.addAction(TimerHelper.TIMER_KILLED);
		filter.addAction(TimerHelper.TIMER_SNOOZE_ACTION);
		filter.addAction(TimerHelper.TIMER_DISMISS_ACTION);
		registerReceiver(mReceiver, filter);
	}

	/**
	 * this is called when a second alarm is triggered while a previous alert
	 * window is still active.
	 */
	@Override
	public void onNewIntent(Intent intent) {
		Log.d(TAG, "onNewIntent");
		super.onNewIntent(intent);

		if (getIntent().hasExtra(AlarmHelper.ALARM_INTENT_EXTRA)) {
			mCurrentType = Constant.TYPE_ALARM;
			mAlarm = getIntent().getParcelableExtra(
					AlarmHelper.ALARM_INTENT_EXTRA);
			mBtnSnooze.setVisibility(View.VISIBLE);
			setTitle();
		} else if (getIntent().hasExtra(TimerHelper.TIMER_INTENT_EXTRA)) {
			mCurrentType = Constant.TYPE_TIMER;
			mTimer = getIntent().getParcelableExtra(
					TimerHelper.TIMER_INTENT_EXTRA);
			mBtnSnooze.setVisibility(View.GONE);
			setTitle();
		}
	}

	@Override
	public void onResume() {
		super.onResume();

		switch (mCurrentType) {
			case Constant.TYPE_ALARM:
				if (AlarmHelper.getAlarm(getContentResolver(), mAlarm.id) == null) {
					mBtnSnooze.setEnabled(false);
				}
				break;
			case Constant.TYPE_TIMER:
				if (TimerHelper.getTimer(getContentResolver(), mTimer.id) == null) {
					mBtnSnooze.setEnabled(false);
				}
				break;
		}

		MobclickAgent.onPageStart(Constant.PAGE_ALERT);
	}

	@Override
	protected void onPause() {
		super.onPause();
		MobclickAgent.onPageEnd(Constant.PAGE_ALERT);
	}

	@Override
	public void onDestroy() {
		super.onDestroy();
		Log.v(TAG, "AlarmAlert.onDestroy()");
		// No longer care about the alarm being killed.
		unregisterReceiver(mReceiver);
	}

	@Override
	public boolean dispatchKeyEvent(KeyEvent event) {
		// Do this on key down to handle a few of the system keys.
		boolean up = event.getAction() == KeyEvent.ACTION_UP;
		switch (event.getKeyCode()) {
			// Volume keys and camera keys dismiss the alarm
			case KeyEvent.KEYCODE_VOLUME_UP:
			case KeyEvent.KEYCODE_VOLUME_DOWN:
			case KeyEvent.KEYCODE_VOLUME_MUTE:
			case KeyEvent.KEYCODE_CAMERA:
			case KeyEvent.KEYCODE_FOCUS:
				if (up) {
					switch (mVolumeBehavior) {
						case VOLUME_BEHAVIOR_SNOOZE:
							snooze();
							return true;
						case VOLUME_BEHAVIOR_DISMISS:
							dismiss(false);
							return true;
					}
				}
				break;
		}
		return super.dispatchKeyEvent(event);
	}

	@Override
	public void onBackPressed() {
		// Don't allow back to dismiss. This method is overriden by AlarmAlert
		// so that the dialog is dismissed.
		return;
	}

	private void initAnalytics() {
		// --------友盟统计
		MobclickAgent.openActivityDurationTrack(false);
		MobclickAgent.updateOnlineConfig(this);
	}

	protected int getLayoutResId() {
		return R.layout.alarm_alert;
	}

	private void updateLayout() {
		setContentView(getLayoutResId());
		mContainer = (ViewGroup) findViewById(R.id.alert_container);
		mTvDate = (TextView) findViewById(R.id.tv_date);
		mTvLabel = (TextView) findViewById(R.id.label_show);
		mBtnSnooze = (Button) findViewById(R.id.snooze);
		mBtnDismiss = (SlideButtonBar) findViewById(R.id.dismiss);

		/*
		 * snooze behavior: pop a snooze confirmation view, kick alarm manager.
		 */
		final String snoozeMinutesStr = PreferenceManager
				.getDefaultSharedPreferences(this).getString(
						SettingsActivity.KEY_ALARM_SNOOZE, DEFAULT_SNOOZE);
		int snoozeMinutes = Integer.parseInt(snoozeMinutesStr);
		mBtnSnooze.setText(getString(R.string.alarm_alert_snooze_text,
				snoozeMinutes));
		mBtnSnooze.requestFocus();
		mBtnSnooze.setOnClickListener(new Button.OnClickListener() {
			public void onClick(View v) {
				snooze();
			}
		});

		if (mCurrentType == Constant.TYPE_TIMER) {
			mBtnSnooze.setVisibility(View.GONE);
		}

		/* dismiss button: close notification */
		mBtnDismiss.setCallback(new Callback() {
			@Override
			public void onBarEndReached() {
				dismiss(false);
			}
		});

		/* Set the title from the passed in alarm */
		setTitle();

		SimpleDateFormat dateformat1 = new SimpleDateFormat("MM-dd E",
				Locale.getDefault());
		String dateString = dateformat1.format(new Date());
		mTvDate.setText(dateString);

		/* Set the theme background according to current time */
		Calendar c = Calendar.getInstance();
		int hour = c.get(Calendar.HOUR_OF_DAY);
		if (hour > 6 && hour <= 10) {
			mContainer.setBackgroundResource(R.drawable.alert_bg_morning);
			mBtnDismiss.setBackgroundResource(R.drawable.slidebar_bg_morning);
		} else if (hour > 10 && hour <= 18) {
			mContainer.setBackgroundResource(R.drawable.alert_bg_noon);
			mBtnDismiss.setBackgroundResource(R.drawable.slidebar_bg_noon);
		} else {
			mContainer.setBackgroundResource(R.drawable.alert_bg_evening);
			mBtnDismiss.setBackgroundResource(R.drawable.slidebar_bg_evening);
		}
	}

	private void setTitle() {
		switch (mCurrentType) {
			case Constant.TYPE_ALARM:
				mTvLabel.setText(mAlarm.label);
				break;
			case Constant.TYPE_TIMER:
				mTvLabel.setText(mTimer.label);
				break;
			default:
				break;
		}
	}

	// Attempt to snooze this alert.
	private void snooze() {
		if (mCurrentType != Constant.TYPE_ALARM) {
			return;
		}
		// Do not snooze if the snooze button is disabled.
		if (!mBtnSnooze.isEnabled()) {
			dismiss(false);
			return;
		}
		final String snooze = PreferenceManager.getDefaultSharedPreferences(
				this).getString(SettingsActivity.KEY_ALARM_SNOOZE,
				DEFAULT_SNOOZE);
		int snoozeMinutes = Integer.parseInt(snooze);

		final long snoozeTime = System.currentTimeMillis()
				+ (1000 * 60 * snoozeMinutes);
		AlarmHelper.saveSnoozeAlert(AlarmAlertFullScreen.this, mAlarm.id,
				snoozeTime);

		// Get the display time for the snooze and update the notification.
		final Calendar c = Calendar.getInstance();
		c.setTimeInMillis(snoozeTime);

		// Append (snoozed) to the label.
		String label = mAlarm.label;
		label = getString(R.string.alarm_notify_snooze_label, label);

		// Notify the user that the alarm has been snoozed.
		Intent cancelSnooze = new Intent(this, AlarmReceiver.class);
		cancelSnooze.setAction(AlarmHelper.CANCEL_SNOOZE);
		cancelSnooze.putExtra(AlarmHelper.ALARM_INTENT_EXTRA, mAlarm);
		PendingIntent broadcast = PendingIntent.getBroadcast(this, mAlarm.id,
				cancelSnooze, 0);
		NotificationManager nm = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
		Notification n = new NotificationCompat.Builder(this)
				.setWhen(0)
				.setAutoCancel(true)
				.setOngoing(true)
				.setTicker(label)
				.setContentTitle(label)
				.setContentText(
						getString(R.string.alarm_notify_snooze_text,
								AlarmHelper.formatTime(this, c)))
				.setContentIntent(broadcast)
				.setSmallIcon(R.drawable.stat_notify_alarm).build();
		nm.notify(mAlarm.id, n);

		String displayTime = getString(R.string.alarm_alert_snooze_set,
				snoozeMinutes);
		// Intentionally log the snooze time for debugging.
		Log.v(TAG, displayTime);

		// Display the snooze minutes in a toast.
		ToastUtil.showLongToast(displayTime);
		stopService(new Intent(AlarmHelper.ALARM_ALERT_ACTION)
				.addCategory(Intent.CATEGORY_DEFAULT));
		HashMap<String, String> params = new HashMap<String, String>();
		params.put(Constant.EVENT_PARAM_SNOOZE_DURATION, snooze);
		MobclickAgent.onEvent(this, Constant.EVENT_SNOOZE, params);
		finish();
	}

	// Dismiss the alarm.
	private void dismiss(boolean killed) {
		Log.i(TAG, killed ? "Alarm killed" : "Alarm dismissed by user");
		// The service told us that the alarm has been killed, do not modify
		// the notification or stop the service.
		if (!killed) {
			// Cancel the notification and stop playing the alarm
			NotificationManager nm = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
			switch (mCurrentType) {
				case Constant.TYPE_ALARM:
					nm.cancel(mAlarm.id);
					stopService(new Intent(AlarmHelper.ALARM_ALERT_ACTION)
							.addCategory(Intent.CATEGORY_DEFAULT));
					break;
				case Constant.TYPE_TIMER:
					nm.cancel(mTimer.getNotifiId());
					stopService(new Intent(TimerHelper.TIMER_ALERT_ACTION)
							.addCategory(Intent.CATEGORY_DEFAULT));
					break;
			}
		}
		finish();
	}

}