package com.avatarqing.alarmclock.activity;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.media.Ringtone;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.avatarqing.alarmclock.adapter.RingtoneListAdapter;
import com.avatarqing.alarmclock.callback.RingtoneCallback;
import com.avatarqing.alarmclock.entity.Alarm;
import com.avatarqing.alarmclock.entity.Alarm.DaysOfWeek;
import com.avatarqing.alarmclock.fragment.SetCustomRingtoneFragment;
import com.avatarqing.alarmclock.fragment.SetRepeatFragment;
import com.avatarqing.alarmclock.fragment.SetSystemRingtoneFragment;
import com.avatarqing.alarmclock.fragment.SetTimeFragment;
import com.avatarqing.alarmclock.util.AlarmHelper;
import com.avatarqing.alarmclock.util.Constant;
import com.avatarqing.alarmclock.util.ToastUtil;
import com.bounique.lib.ad.AdUtils;
import com.bounique.tools.alarmclock.R;
import com.boutique.lib.analytics.AnalyticUtils;
import com.nineoldandroids.view.ViewPropertyAnimator;
import com.umeng.analytics.MobclickAgent;

import net.simonvt.timepicker.TimePicker;
import net.simonvt.timepicker.TimePicker.OnTimeChangedListener;

import java.util.HashMap;

public class SetAlarmActivity extends BaseActivity implements
		View.OnClickListener, OnTimeChangedListener, TextWatcher,
		SetRepeatFragment.Callbacks, RingtoneCallback {
	private static final String TAG = "SetAlarm";

	private static final String KEY_CURRENT_ALARM = "currentAlarm";
	private static final String KEY_ORIGINAL_ALARM = "originalAlarm";

	// 各视图控件
	private View mBtnRepeat = null;
	private View mBtnRingtone = null;
	private View mBtnTimePick = null;

	private TextView mTvRepeat = null;
	private TextView mTvRingtone = null;
	private TextView mTvTime = null;

	private Button mBtnSave = null;
	// private Button mBtnDelete = null;
	private Button mBtnRevert = null;

	private View mBottomBar = null;

	// 闹钟实体类参数
	private Alarm mOriginalAlarm = null;
	private int mId = Alarm.Columns.INVALID_ALARM_ID;
	private int mHour;
	private int mMinute;
	private DaysOfWeek mDaysOfWeek = null;
	private String mLabel = null;
	private boolean mIsVibrate = true;
	private boolean mIsSilent = false;
	private Uri mRingtone = null;

	private final int mAnimationDuration = 1000;

	@Override
	protected void onCreate(Bundle icicle) {
		Log.i(TAG, "onCreate");
		super.onCreate(icicle);
		MobclickAgent.updateOnlineConfig(this);

		setContentView(R.layout.set_alarm);
		findViews();

		Alarm alarm = getIntent().getParcelableExtra(
				AlarmHelper.ALARM_INTENT_EXTRA);
		if (alarm == null) {
			alarm = new Alarm();
		}
		mOriginalAlarm = alarm;

		// Populate the views with the original alarm data. updateViewValues
		// also
		// sets mId so it must be called before checking mId below.
		updateViewValues(mOriginalAlarm);

		initViewSetting();

		showSetTimePage();

		boolean enableVibrate = PreferenceManager.getDefaultSharedPreferences(
				this).getBoolean(SettingsActivity.KEY_ALARM_VIBRATE, true);
		mIsVibrate = enableVibrate;

		AdUtils.showAllAd(this);
	}

	@Override
	protected void onResume() {
		Log.i(TAG, "onResume");
		super.onResume();
		MobclickAgent.onPageStart(Constant.PAGE_SET_ALARM);
	}

	@Override
	protected void onPause() {
		Log.i(TAG, "onPause");
		super.onPause();
		MobclickAgent.onPageEnd(Constant.PAGE_SET_ALARM);
	}

	@Override
	protected void onDestroy() {
		Log.i(TAG, "onDestroy");
		super.onDestroy();
	}

	@Override
	protected void onSaveInstanceState(Bundle outState) {
		super.onSaveInstanceState(outState);
		outState.putParcelable(KEY_ORIGINAL_ALARM, mOriginalAlarm);
		outState.putParcelable(KEY_CURRENT_ALARM, buildAlarmFromUi());
	}

	@Override
	protected void onRestoreInstanceState(Bundle state) {
		super.onRestoreInstanceState(state);

		Alarm alarmFromBundle = state.getParcelable(KEY_ORIGINAL_ALARM);
		if (alarmFromBundle != null) {
			mOriginalAlarm = alarmFromBundle;
		}

		alarmFromBundle = state.getParcelable(KEY_CURRENT_ALARM);
		if (alarmFromBundle != null) {
			updateViewValues(alarmFromBundle);
		}
	}

	@Override
	public void onBackPressed() {
		super.onBackPressed();
		overridePendingTransition(R.anim.close_enter, R.anim.close_exit);
	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
			case R.id.alarm_save:
				saveAlarm(null);
				finish();
				overridePendingTransition(R.anim.close_enter, R.anim.close_exit);
				break;
			// case R.id.alarm_delete:
			// deleteAlarm();
			// break;
			case R.id.alarm_cancel:
				finish();
				overridePendingTransition(R.anim.close_enter, R.anim.close_exit);
				break;
			case R.id.btn_repeat:
				showSetRepeatPage();
				hideBottomBar();
				break;
			case R.id.btn_ringtone:
				new AlertDialog.Builder(this)
						.setTitle(R.string.choose_ringtone_title)
						.setAdapter(new RingtoneListAdapter(this),
								new DialogInterface.OnClickListener() {
									@Override
									public void onClick(DialogInterface dialog,
									                    int which) {
										switch (which) {
											case 0:
												// 静音
												onRingtoneSelected(null);
												mIsVibrate = true;
												// TODO 清除铃声选择条目
												break;
											case 1:
												// 系统铃声
												showSetSystemRingtonePage();
												hideBottomBar();
												break;
											case 2:
												// 外存铃声
												showSetCustomRingtonePage();
												hideBottomBar();
												break;
										}
									}
								}).show();
				break;
			case R.id.btn_timepick:
				showSetTimePage();
				showBottomBar();
				break;
		}
	}

	@Override
	public void beforeTextChanged(CharSequence s, int start, int count,
	                              int after) {

	}

	@Override
	public void onTextChanged(CharSequence s, int start, int before, int count) {

	}

	@Override
	public void afterTextChanged(Editable s) {
		mLabel = s.toString();
	}

	@Override
	public void onTimeChanged(TimePicker view, int hourOfDay, int minute,
	                          int second) {
		mHour = hourOfDay;
		mMinute = minute;
		mTvTime.setText(AlarmHelper.formatTime(this, mHour, mMinute,
				mDaysOfWeek));
	}

	@Override
	public void onRepeatSetChanged(DaysOfWeek newDaysOfWeek) {
		mDaysOfWeek = new DaysOfWeek(newDaysOfWeek.getCoded());
		mTvRepeat.setText(mDaysOfWeek.toString(this, true));
	}

	@Override
	public void onRingtoneSelected(Uri uri) {
		mRingtone = uri;
		updateRingtoneView(uri);
	}

	private void showBottomBar() {
		ViewPropertyAnimator.animate(mBottomBar)
				.setDuration(mAnimationDuration).translationY(0);
	}

	private void hideBottomBar() {
		ViewPropertyAnimator.animate(mBottomBar)
				.setDuration(mAnimationDuration)
				.translationY(mBottomBar.getHeight());
	}

	private void findViews() {
		mTvTime = (TextView) findViewById(R.id.tv_timepick);
		mTvRingtone = (TextView) findViewById(R.id.tv_ringtone);
		mTvRepeat = (TextView) findViewById(R.id.tv_repeat_day);
		mBtnRepeat = findViewById(R.id.btn_repeat);
		mBtnTimePick = findViewById(R.id.btn_timepick);
		mBtnRingtone = findViewById(R.id.btn_ringtone);
		mBtnSave = (Button) findViewById(R.id.alarm_save);
		// mBtnDelete = (Button) findViewById(R.id.alarm_delete);
		mBtnRevert = (Button) findViewById(R.id.alarm_cancel);
		mBottomBar = findViewById(R.id.alarm_operation_bar);
	}

	/** 初始化各视图设置 */
	private void initViewSetting() {
		mBtnRepeat.setOnClickListener(this);
		mBtnTimePick.setOnClickListener(this);
		mBtnRingtone.setOnClickListener(this);
		mBtnSave.setOnClickListener(this);
		mBtnRevert.setOnClickListener(this);
		// mBtnDelete.setOnClickListener(this);
		// if (mId == Alarm.Columns.INVALID_ALARM_ID) {
		// mBtnDelete.setEnabled(false);
		// mBtnDelete.setVisibility(View.GONE);
		// }
	}

	private void updateViewValues(Alarm alarm) {
		mId = alarm.id;
		mHour = alarm.hour;
		mMinute = alarm.minutes;
		mDaysOfWeek = alarm.daysOfWeek;
		mIsVibrate = alarm.vibrate;
		mLabel = alarm.label;
		mRingtone = alarm.alert;

		mTvRepeat.setText(alarm.daysOfWeek.toString(this, true));
		mTvTime.setText(AlarmHelper.formatTime(this, mHour, mMinute,
				mDaysOfWeek));
		updateRingtoneView(mRingtone);
	}

	private Alarm buildAlarmFromUi() {
		Alarm alarm = new Alarm();
		alarm.enabled = mOriginalAlarm.enabled;
		alarm.id = mId;
		alarm.hour = mHour;
		alarm.minutes = mMinute;
		alarm.daysOfWeek = mDaysOfWeek;
		alarm.vibrate = mIsVibrate;
		alarm.label = mLabel;
		alarm.alert = mRingtone;
		alarm.silent = mIsSilent;
		return alarm;
	}

	private void updateRingtoneView(Uri uri) {
		if (uri != null) {
			final Ringtone r = RingtoneManager.getRingtone(
					SetAlarmActivity.this, uri);
			if (r != null) {
				mTvRingtone.setText(r.getTitle(SetAlarmActivity.this));
			}
		} else {
			mTvRingtone.setText(getString(R.string.silent));
		}
	}

	private void showSetTimePage() {
		if (null == getSupportFragmentManager().findFragmentByTag(
				SetTimeFragment.TAG)) {
			Bundle args = new Bundle();
			args.putInt(Constant.ARG_HOUR, mHour);
			args.putInt(Constant.ARG_MINUTE, mMinute);
			args.putInt(Constant.ARG_SECOND, 0);
			args.putInt(Constant.ARG_TIMEPICK_MODE, TimePicker.MODE_TIME_PICK);
			args.putBoolean(Constant.ARG_TIMEPICK_SECOND_ENABLED, false);
			args.putString(Constant.ARG_LABEL, mLabel);
			Fragment setTimeFragment = Fragment.instantiate(this,
					SetTimeFragment.class.getName(), args);
			getSupportFragmentManager()
					.beginTransaction()
					.setCustomAnimations(R.anim.translate_up_open,
							R.anim.translate_up_exit)
					.replace(R.id.set_alarm_content_frame, setTimeFragment,
							SetTimeFragment.TAG).commit();

			mBtnRepeat.setBackgroundResource(R.drawable.btn_grey_bg);
			mBtnRingtone.setBackgroundResource(R.drawable.btn_grey_bg);
			mBtnTimePick.setBackgroundColor(getResources().getColor(
					R.color.grey3_translucent2));
		}
	}

	private void showSetSystemRingtonePage() {
		if (null == getSupportFragmentManager().findFragmentByTag(
				SetSystemRingtoneFragment.TAG)) {
			Bundle ringtoneArgs = new Bundle();
			ringtoneArgs.putParcelable(Constant.ARG_RINGTONE, mRingtone);
			Fragment setRingtoneFragment = Fragment.instantiate(this,
					SetSystemRingtoneFragment.class.getName(), ringtoneArgs);

			getSupportFragmentManager()
					.beginTransaction()
					.setCustomAnimations(R.anim.translate_up_open,
							R.anim.translate_up_exit)
					.replace(R.id.set_alarm_content_frame, setRingtoneFragment,
							SetSystemRingtoneFragment.TAG).commit();

			mBtnRepeat.setBackgroundResource(R.drawable.btn_grey_bg);
			mBtnRingtone.setBackgroundColor(getResources().getColor(
					R.color.grey3_translucent2));
			mBtnTimePick.setBackgroundResource(R.drawable.btn_grey_bg);
		}
	}

	private void showSetCustomRingtonePage() {
		if (null == getSupportFragmentManager().findFragmentByTag(
				SetCustomRingtoneFragment.TAG)) {
			Bundle ringtoneArgs = new Bundle();
			ringtoneArgs.putParcelable(Constant.ARG_RINGTONE, mRingtone);
			Fragment setRingtoneFragment = Fragment.instantiate(this,
					SetCustomRingtoneFragment.class.getName(), ringtoneArgs);

			getSupportFragmentManager()
					.beginTransaction()
					.setCustomAnimations(R.anim.translate_up_open,
							R.anim.translate_up_exit)
					.replace(R.id.set_alarm_content_frame, setRingtoneFragment,
							SetCustomRingtoneFragment.TAG).commit();

			mBtnRepeat.setBackgroundResource(R.drawable.btn_grey_bg);
			mBtnRingtone.setBackgroundColor(getResources().getColor(
					R.color.grey3_translucent2));
			mBtnTimePick.setBackgroundResource(R.drawable.btn_grey_bg);
		}
	}

	private void showSetRepeatPage() {
		if (null == getSupportFragmentManager().findFragmentByTag(
				SetRepeatFragment.TAG)) {
			Bundle repeatArgs = new Bundle();
			repeatArgs.putParcelable(Constant.ARG_DAYSOFWEEK, mDaysOfWeek);
			Fragment setRepeatFragment = Fragment.instantiate(this,
					SetRepeatFragment.class.getName(), repeatArgs);
			getSupportFragmentManager()
					.beginTransaction()
					.setCustomAnimations(R.anim.translate_up_open,
							R.anim.translate_up_exit)
					.replace(R.id.set_alarm_content_frame, setRepeatFragment,
							SetRepeatFragment.TAG).commit();

			mBtnRepeat.setBackgroundColor(getResources().getColor(
					R.color.grey3_translucent2));
			mBtnRingtone.setBackgroundResource(R.drawable.btn_grey_bg);
			mBtnTimePick.setBackgroundResource(R.drawable.btn_grey_bg);
		}
	}

	private long saveAlarm(Alarm alarm) {
		if (alarm == null) {
			alarm = buildAlarmFromUi();
		}

		long time;
		if (alarm.id == Alarm.Columns.INVALID_ALARM_ID) {
			alarm.enabled = true;
			time = AlarmHelper.addAlarm(this, alarm);
			// popAlarmSetToast(SetAlarmActivity.this, time);

			// addAlarm populates the alarm with the new id. Update mId so that
			// changes to other preferences update the new alarm.
			mId = alarm.id;
		} else {
			time = AlarmHelper.setAlarm(this, alarm);
		}
		analytic(alarm.hour, alarm.minutes, alarm.daysOfWeek);
		return time;
	}

	/** 统计信息 */
	private void analytic(int hour, int minute, DaysOfWeek daysOfWeek) {
		HashMap<String, String> params = new HashMap<String, String>();
		String timePeriod = null;
		long value = -1;
		switch (hour) {
			case 0:
				timePeriod = Constant.TIME_0_1;
				value = 0;
				break;
			case 1:
				timePeriod = Constant.TIME_1_2;
				value = 1;
				break;
			case 2:
				timePeriod = Constant.TIME_2_3;
				value = 2;
				break;
			case 3:
				timePeriod = Constant.TIME_3_4;
				value = 3;
				break;
			case 4:
				timePeriod = Constant.TIME_4_5;
				value = 4;
				break;
			case 5:
				timePeriod = Constant.TIME_5_6;
				value = 5;
				break;
			case 6:
				timePeriod = Constant.TIME_6_7;
				value = 6;
				break;
			case 7:
				timePeriod = Constant.TIME_7_8;
				value = 7;
				break;
			case 8:
				timePeriod = Constant.TIME_8_9;
				value = 8;
				break;
			case 9:
				timePeriod = Constant.TIME_9_10;
				value = 9;
				break;
			case 10:
				timePeriod = Constant.TIME_10_11;
				value = 10;
				break;
			case 11:
				timePeriod = Constant.TIME_11_12;
				value = 11;
				break;
			case 12:
				timePeriod = Constant.TIME_12_13;
				value = 12;
				break;
			case 13:
				timePeriod = Constant.TIME_13_14;
				value = 13;
				break;
			case 14:
				timePeriod = Constant.TIME_14_15;
				value = 14;
				break;
			case 15:
				timePeriod = Constant.TIME_15_16;
				value = 15;
				break;
			case 16:
				timePeriod = Constant.TIME_16_17;
				value = 16;
				break;
			case 17:
				timePeriod = Constant.TIME_17_18;
				value = 17;
				break;
			case 18:
				timePeriod = Constant.TIME_18_19;
				value = 18;
				break;
			case 19:
				timePeriod = Constant.TIME_19_20;
				value = 19;
				break;
			case 20:
				timePeriod = Constant.TIME_20_21;
				value = 20;
				break;
			case 21:
				timePeriod = Constant.TIME_21_22;
				value = 21;
				break;
			case 22:
				timePeriod = Constant.TIME_22_23;
				value = 22;
				break;
			case 23:
				timePeriod = Constant.TIME_23_24;
				value = 23;
				break;
		}
		AnalyticUtils.sendGaEvent(this, Constant.GA_CATEGORY_ALARM_SET,
				Constant.GA_ACTION_HOUR_SET, timePeriod, value);

		String repeatDays = daysOfWeek.toStringForAnalytic(this);
		AnalyticUtils.sendGaEvent(this, Constant.GA_CATEGORY_ALARM_SET,
				Constant.GA_ACTION_REPEAT_SET, repeatDays, null);

		params.put(Constant.EVENT_PARAM_ALARM_HOUR_PERIOD, timePeriod);
		params.put(Constant.EVENT_PARAM_REPEAT, repeatDays);
		AnalyticUtils.sendUmengEvent(this, Constant.EVENT_SET_ALARM, params);
	}

	// private void deleteAlarm() {
	// if (mId != Alarm.Columns.INVALID_ALARM_ID) {
	// new AlertDialog.Builder(this)
	// .setTitle(getString(R.string.delete_alarm))
	// .setMessage(getString(R.string.delete_alarm_confirm))
	// .setPositiveButton(android.R.string.ok,
	// new DialogInterface.OnClickListener() {
	// public void onClick(DialogInterface d, int w) {
	// AlarmHelper.deleteAlarm(
	// SetAlarmActivity.this, mId);
	// finish();
	// overridePendingTransition(
	// R.anim.close_enter,
	// R.anim.close_exit);
	// }
	// }).setNegativeButton(android.R.string.cancel, null)
	// .show();
	// }
	// }

	public static void popAlarmSetToast(Context context, Alarm alarm) {
		popAlarmSetToast(context, alarm.hour, alarm.minutes, alarm.daysOfWeek);
	}

	/**
	 * Display a toast that tells the user how long until the alarm goes off.
	 * This helps prevent "am/pm" mistakes.
	 */
	private static void popAlarmSetToast(Context context, int hour, int minute,
	                                     Alarm.DaysOfWeek daysOfWeek) {
		popAlarmSetToast(context,
				AlarmHelper.calculateAlarmTime(hour, minute, daysOfWeek)
						.getTimeInMillis());
	}

	private static void popAlarmSetToast(Context context, long timeInMillis) {
		String toastText = formatToast(context, timeInMillis);
		ToastUtil.showLongToast(toastText);
	}

	/**
	 * format "Alarm set for 2 days 7 hours and 53 minutes from now"
	 * 将给定的毫秒数转化为日、时、分的格式
	 */
	private static String formatToast(Context context, long timeInMillis) {
		long delta = timeInMillis - System.currentTimeMillis();
		long hours = delta / (1000 * 60 * 60);
		long minutes = delta / (1000 * 60) % 60;
		long days = hours / 24;
		hours = hours % 24;

		String daySeq = (days == 0) ? "" : (days == 1) ? context
				.getString(R.string.day) : context.getString(R.string.days,
				Long.toString(days));

		String minSeq = (minutes == 0) ? "" : (minutes == 1) ? context
				.getString(R.string.minute) : context.getString(
				R.string.minutes, Long.toString(minutes));

		String hourSeq = (hours == 0) ? "" : (hours == 1) ? context
				.getString(R.string.hour) : context.getString(R.string.hours,
				Long.toString(hours));

		boolean dispDays = days > 0;
		boolean dispHour = hours > 0;
		boolean dispMinute = minutes > 0;

		int index = (dispDays ? 1 : 0) | (dispHour ? 2 : 0)
				| (dispMinute ? 4 : 0);

		String[] formats = context.getResources().getStringArray(
				R.array.alarm_set);
		return String.format(formats[index], daySeq.trim(), hourSeq.trim(),
				minSeq.trim());
	}
}