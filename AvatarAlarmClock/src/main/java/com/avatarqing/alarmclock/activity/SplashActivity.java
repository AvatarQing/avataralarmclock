package com.avatarqing.alarmclock.activity;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.util.DisplayMetrics;
import android.widget.TextView;

import com.bounique.tools.alarmclock.R;
import com.nineoldandroids.view.ViewHelper;
import com.nineoldandroids.view.ViewPropertyAnimator;

public class SplashActivity extends BaseActivity {

	private TextView mTitle = null;

	private Handler mHandler = new Handler();
	private final int mTaskDelayMillis = 3000;
	private final int mAnimDuration = 2000;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.welcome);
		mTitle = (TextView) findViewById(R.id.tv_app_intro);

		mHandler.postDelayed(mTask, mTaskDelayMillis);

		DisplayMetrics dm = new DisplayMetrics();
		getWindowManager().getDefaultDisplay().getMetrics(dm);
		// 根据屏幕尺寸调整标语位置
		ViewHelper.setY(mTitle, dm.heightPixels / 10 * 7);

		ViewHelper.setAlpha(mTitle, 0);

		ViewPropertyAnimator.animate(mTitle).setDuration(mAnimDuration)
				.alpha(1);
	}

	@Override
	public void onBackPressed() {
		// 屏蔽返回键
	}

	private Runnable mTask = new Runnable() {
		@Override
		public void run() {
			Intent intent = new Intent(SplashActivity.this, MainActivity.class);
			startActivity(intent);
			SplashActivity.this.finish();
		}
	};
}