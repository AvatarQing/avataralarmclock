package com.avatarqing.alarmclock.activity;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.media.Ringtone;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.avatarqing.alarmclock.adapter.RingtoneListAdapter;
import com.avatarqing.alarmclock.callback.RingtoneCallback;
import com.avatarqing.alarmclock.entity.Timer;
import com.avatarqing.alarmclock.fragment.SetCustomRingtoneFragment;
import com.avatarqing.alarmclock.fragment.SetSystemRingtoneFragment;
import com.avatarqing.alarmclock.fragment.SetTimeFragment;
import com.avatarqing.alarmclock.util.Constant;
import com.avatarqing.alarmclock.util.TimerHelper;
import com.avatarqing.alarmclock.util.ToastUtil;
import com.bounique.lib.ad.AdUtils;
import com.bounique.tools.alarmclock.R;
import com.nineoldandroids.view.ViewPropertyAnimator;
import com.umeng.analytics.MobclickAgent;

import net.simonvt.timepicker.TimePicker;
import net.simonvt.timepicker.TimePicker.OnTimeChangedListener;

import java.util.HashMap;
import java.util.Locale;

public class SetTimerActivity extends BaseActivity implements
		View.OnClickListener, OnTimeChangedListener, TextWatcher,
		RingtoneCallback {
	private static final String TAG = "SetAlarm";

	private static final String KEY_CURRENT_TIMER = "currentTimer";
	private static final String KEY_ORIGINAL_TIMER = "originalTimer";

	// 各视图控件
	private View mBtnRingtone = null;
	private View mBtnTimePick = null;

	private TextView mTvRingtone = null;
	private TextView mTvTime = null;

	private Button mBtnSave = null;
	private Button mBtnRevert = null;

	private View mBottomBar = null;

	// 闹钟实体类参数
	private Timer mOriginalTimer = null;
	private int mId = Timer.Columns.INVALID_TIMER_ID;
	private int mHour;
	private int mMinute;
	private int mSecond;
	private String mLabel = null;
	private boolean mIsVibrate = true;
	private boolean mIsSilent = false;
	private Uri mRingtone = null;

	private final int mAnimationDuration = 1000;

	@Override
	protected void onCreate(Bundle icicle) {
		Log.i(TAG, "onCreate");
		super.onCreate(icicle);
		MobclickAgent.updateOnlineConfig(this);

		setContentView(R.layout.set_timer);

		findViews();

		Timer timer = getIntent().getParcelableExtra(
				TimerHelper.TIMER_INTENT_EXTRA);
		if (timer == null) {
			timer = new Timer();
		}
		mOriginalTimer = timer;

		updateViewValues(mOriginalTimer);

		initViewSetting();

		showSetTimePage();

		boolean enableVibrate = PreferenceManager.getDefaultSharedPreferences(
				this).getBoolean(SettingsActivity.KEY_ALARM_VIBRATE, true);
		mIsVibrate = enableVibrate;

		AdUtils.showAllAd(this);
	}

	@Override
	protected void onResume() {
		Log.i(TAG, "onResume");
		super.onResume();
		MobclickAgent.onPageStart(Constant.PAGE_SET_TIMER);
	}

	@Override
	protected void onPause() {
		Log.i(TAG, "onPause");
		super.onPause();
		MobclickAgent.onPageEnd(Constant.PAGE_SET_TIMER);
	}

	@Override
	protected void onDestroy() {
		Log.i(TAG, "onDestroy");
		super.onDestroy();
	}

	@Override
	protected void onSaveInstanceState(Bundle outState) {
		super.onSaveInstanceState(outState);
		outState.putParcelable(KEY_ORIGINAL_TIMER, mOriginalTimer);
		outState.putParcelable(KEY_CURRENT_TIMER, buildTimerFromUi());
	}

	@Override
	protected void onRestoreInstanceState(Bundle state) {
		super.onRestoreInstanceState(state);

		Timer timerFromBundle = state.getParcelable(KEY_ORIGINAL_TIMER);
		if (timerFromBundle != null) {
			mOriginalTimer = timerFromBundle;
		}

		timerFromBundle = state.getParcelable(KEY_CURRENT_TIMER);
		if (timerFromBundle != null) {
			updateViewValues(timerFromBundle);
		}
	}

	@Override
	public void onBackPressed() {
		super.onBackPressed();
		overridePendingTransition(R.anim.close_enter, R.anim.close_exit);
	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
			case R.id.timer_save:
				saveTimer(null);
				finish();
				overridePendingTransition(R.anim.close_enter, R.anim.close_exit);
				break;
			case R.id.timer_cancel:
				finish();
				overridePendingTransition(R.anim.close_enter, R.anim.close_exit);
				break;
			case R.id.btn_ringtone:
				new AlertDialog.Builder(this)
						.setTitle(R.string.choose_ringtone_title)
						.setAdapter(new RingtoneListAdapter(this),
								new DialogInterface.OnClickListener() {
									@Override
									public void onClick(DialogInterface dialog,
									                    int which) {
										switch (which) {
											case 0:
												// 静音
												onRingtoneSelected(null);
												mIsVibrate = true;
												// TODO 清除铃声选择条目
												break;
											case 1:
												// 系统铃声
												showSetSystemRingtonePage();
												hideBottomBar();
												break;
											case 2:
												// 外存铃声
												showSetCustomRingtonePage();
												hideBottomBar();
												break;
										}
									}
								}).show();
				break;
			case R.id.btn_timepick:
				showSetTimePage();
				showBottomBar();
				break;
		}
	}

	@Override
	public void beforeTextChanged(CharSequence s, int start, int count,
	                              int after) {

	}

	@Override
	public void onTextChanged(CharSequence s, int start, int before, int count) {

	}

	@Override
	public void afterTextChanged(Editable s) {
		mLabel = s.toString();
	}

	@Override
	public void onTimeChanged(TimePicker view, int hourOfDay, int minute,
	                          int second) {
		mHour = hourOfDay;
		mMinute = minute;
		mSecond = second;
		mTvTime.setText(formatTime(hourOfDay, minute, second));
	}

	@Override
	public void onRingtoneSelected(Uri uri) {
		mRingtone = uri;
		updateRingtoneView(uri);
	}

	private void showBottomBar() {
		ViewPropertyAnimator.animate(mBottomBar)
				.setDuration(mAnimationDuration).translationY(0);
	}

	private void hideBottomBar() {
		ViewPropertyAnimator.animate(mBottomBar)
				.setDuration(mAnimationDuration)
				.translationY(mBottomBar.getHeight());
	}

	private void findViews() {
		mTvTime = (TextView) findViewById(R.id.tv_timepick);
		mTvRingtone = (TextView) findViewById(R.id.tv_ringtone);
		mBtnTimePick = findViewById(R.id.btn_timepick);
		mBtnRingtone = findViewById(R.id.btn_ringtone);
		mBtnSave = (Button) findViewById(R.id.timer_save);
		mBtnRevert = (Button) findViewById(R.id.timer_cancel);
		mBottomBar = findViewById(R.id.timer_operation_bar);
	}

	/** 初始化各视图设置 */
	private void initViewSetting() {
		mBtnTimePick.setOnClickListener(this);
		mBtnRingtone.setOnClickListener(this);
		mBtnSave.setOnClickListener(this);
		mBtnRevert.setOnClickListener(this);
	}

	private void updateViewValues(Timer timer) {
		mId = timer.id;
		mHour = timer.hour;
		mMinute = timer.minutes;
		mSecond = timer.second;
		mIsVibrate = timer.vibrate;
		mLabel = timer.label;
		mRingtone = timer.alert;

		mTvTime.setText(formatTime(mHour, mMinute, mSecond));
		updateRingtoneView(mRingtone);
	}

	private Timer buildTimerFromUi() {
		Timer timer = new Timer();
		timer.enabled = mOriginalTimer.enabled;
		timer.id = mId;
		timer.hour = mHour;
		timer.minutes = mMinute;
		timer.second = mSecond;
		timer.vibrate = mIsVibrate;
		timer.label = mLabel;
		timer.alert = mRingtone;
		timer.silent = mIsSilent;
		return timer;
	}

	private void updateRingtoneView(Uri uri) {
		if (uri != null) {
			final Ringtone r = RingtoneManager.getRingtone(
					SetTimerActivity.this, uri);
			if (r != null) {
				mTvRingtone.setText(r.getTitle(SetTimerActivity.this));
			}
		} else {
			mTvRingtone.setText(getString(R.string.silent));
		}
	}

	private void showSetTimePage() {
		if (null == getSupportFragmentManager().findFragmentByTag(
				SetTimeFragment.TAG)) {

			Bundle args = new Bundle();
			args.putInt(Constant.ARG_HOUR, mHour);
			args.putInt(Constant.ARG_MINUTE, mMinute);
			args.putInt(Constant.ARG_SECOND, mSecond);
			args.putInt(Constant.ARG_TIMEPICK_MODE, TimePicker.MODE_PERIOD_PICK);
			args.putBoolean(Constant.ARG_TIMEPICK_SECOND_ENABLED, true);
			args.putString(Constant.ARG_LABEL, mLabel);
			Fragment setTimeFragment = Fragment.instantiate(this,
					SetTimeFragment.class.getName(), args);
			getSupportFragmentManager()
					.beginTransaction()
					.setCustomAnimations(R.anim.translate_up_open,
							R.anim.translate_up_exit)
					.replace(R.id.set_timer_content_frame, setTimeFragment,
							SetTimeFragment.TAG).commit();

			mBtnRingtone.setBackgroundResource(R.drawable.btn_grey_bg);
			mBtnTimePick.setBackgroundColor(getResources().getColor(
					R.color.grey3_translucent2));
		}
	}

	private void showSetSystemRingtonePage() {
		if (null == getSupportFragmentManager().findFragmentByTag(
				SetSystemRingtoneFragment.TAG)) {
			Bundle ringtoneArgs = new Bundle();
			ringtoneArgs.putParcelable(Constant.ARG_RINGTONE, mRingtone);
			Fragment setRingtoneFragment = Fragment.instantiate(this,
					SetSystemRingtoneFragment.class.getName(), ringtoneArgs);

			getSupportFragmentManager()
					.beginTransaction()
					.setCustomAnimations(R.anim.translate_up_open,
							R.anim.translate_up_exit)
					.replace(R.id.set_timer_content_frame, setRingtoneFragment,
							SetSystemRingtoneFragment.TAG).commit();

			mBtnRingtone.setBackgroundColor(getResources().getColor(
					R.color.grey3_translucent2));
			mBtnTimePick.setBackgroundResource(R.drawable.btn_grey_bg);
		}
	}

	private void showSetCustomRingtonePage() {
		if (null == getSupportFragmentManager().findFragmentByTag(
				SetCustomRingtoneFragment.TAG)) {
			Bundle ringtoneArgs = new Bundle();
			ringtoneArgs.putParcelable(Constant.ARG_RINGTONE, mRingtone);
			Fragment setRingtoneFragment = Fragment.instantiate(this,
					SetCustomRingtoneFragment.class.getName(), ringtoneArgs);

			getSupportFragmentManager()
					.beginTransaction()
					.setCustomAnimations(R.anim.translate_up_open,
							R.anim.translate_up_exit)
					.replace(R.id.set_timer_content_frame, setRingtoneFragment,
							SetCustomRingtoneFragment.TAG).commit();

			mBtnRingtone.setBackgroundColor(getResources().getColor(
					R.color.grey3_translucent2));
			mBtnTimePick.setBackgroundResource(R.drawable.btn_grey_bg);
		}
	}

	private long saveTimer(Timer timer) {
		long time = 0;
		if (timer == null) {
			timer = buildTimerFromUi();
		}

		if (timer.id == Timer.Columns.INVALID_TIMER_ID) {
			timer.enabled = true;
			time = TimerHelper.addTimer(this, timer);
			// popTimerSetToast(SetTimerActivity.this, time);

			// addAlarm populates the alarm with the new id. Update mId so that
			// changes to other preferences update the new alarm.
			mId = timer.id;
		} else {
			time = TimerHelper.setTimer(this, timer);
		}

		HashMap<String, String> params = new HashMap<String, String>();
		params.put(Constant.EVENT_PARAM_TIMER_DURATION, timer.hour + ":"
				+ timer.minutes + ":" + timer.second);
		MobclickAgent.onEvent(this, Constant.EVENT_SET_TIMER, params);
		return time;
	}

	private String formatTime(int hourOfDay, int minute, int second) {
		return String.format(Locale.getDefault(), Constant.FORMAT_TIME,
				hourOfDay, minute, second);
	}

	/**
	 * Display a toast that tells the user how long until the alarm goes off.
	 */
	public static void popTimerSetToast(Context context, Timer timer) {
		popTimerSetToast(context, TimerHelper.calculateRemainTime(timer));
	}

	private static void popTimerSetToast(Context context, long timeInMillis) {
		String toastText = formatToast(context, timeInMillis);
		ToastUtil.showLongToast(toastText);
	}

	private static String formatToast(Context context, long timeInMillis) {
		long delta = timeInMillis - System.currentTimeMillis();
		long hours = delta / (1000 * 60 * 60);
		long minutes = delta / (1000 * 60) % 60;
		long seconds = delta / 1000 % 60;
		hours = hours % 24;

		String secSeq = (seconds == 0) ? "" : (seconds == 1) ? context
				.getString(R.string.second) : context.getString(
				R.string.seconds, Long.toString(seconds));

		String minSeq = (minutes == 0) ? "" : (minutes == 1) ? context
				.getString(R.string.minute) : context.getString(
				R.string.minutes, Long.toString(minutes));

		String hourSeq = (hours == 0) ? "" : (hours == 1) ? context
				.getString(R.string.hour) : context.getString(R.string.hours,
				Long.toString(hours));

		boolean dispHour = hours > 0;
		boolean dispMinute = minutes > 0;
		boolean dispSeconds = seconds > 0;

		int index = (dispHour ? 1 : 0) | (dispMinute ? 2 : 0)
				| (dispSeconds ? 4 : 0);

		String[] formats = context.getResources().getStringArray(
				R.array.timer_set);
		return String.format(formats[index], hourSeq, minSeq, secSeq);
	}

}