package com.avatarqing.alarmclock.activity;

import android.os.Bundle;
import android.preference.Preference;
import android.preference.PreferenceScreen;
import android.text.TextUtils;

import com.avatarqing.alarmclock.util.Const;
import com.avatarqing.alarmclock.util.Utils;
import com.bounique.lib.ad.AdUtils;
import com.bounique.tools.alarmclock.R;

@SuppressWarnings("deprecation")
public class AboutUsActivity extends BasePreferenceActivity {

	public static final String PREF_BTN_RATE = "pref_btn_rate";
	public static final String PREF_BTN_SHARE = "pref_btn_share";
	public static final String PREF_BTN_FEEDBACK = "pref_btn_feedback";

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		addPreferencesFromResource(R.xml.about_us);
		AdUtils.showAllAd(this);
	}

	@Override
	public boolean onPreferenceTreeClick(PreferenceScreen preferenceScreen,
	                                     Preference preference) {
		String key = preference.getKey();
		if (!TextUtils.isEmpty(key)) {
			if (key.equals(PREF_BTN_RATE)) {
				doActionRate();
				return true;
			} else if (key.equals(PREF_BTN_SHARE)) {
				doActionShare();
				return true;
			} else if (key.equals(PREF_BTN_FEEDBACK)) {
				doActionFeedback();
				return true;
			}
		}
		return super.onPreferenceTreeClick(preferenceScreen, preference);
	}

	/** 给软件评分 */
	private void doActionRate() {
		String linkInPlayStore = Const.GOOGLE_PLAY_PREFFIX_HTTPS
				+ getPackageName();
		Utils.openLink(getApplicationContext(), linkInPlayStore);
	}

	/** 分享软件给朋友 */
	private void doActionShare() {
		String linkInPlayStore = Const.GOOGLE_PLAY_PREFFIX_HTTPS
				+ getPackageName();
		String subject = getString(R.string.share_subject);
		String text = getString(R.string.share_content) + linkInPlayStore;
		Utils.shareText(getApplicationContext(), subject, text);
	}

	/** 意见反馈 */
	private void doActionFeedback() {
		String[] receivers = new String[] { getString(R.string.developer_email) };
		String subject = getString(R.string.feedback_subject);
		String content = null;
		Utils.sendTextEmail(getApplicationContext(), receivers, subject,
				content);
	}
}