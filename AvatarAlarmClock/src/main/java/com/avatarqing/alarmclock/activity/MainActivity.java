package com.avatarqing.alarmclock.activity;

import android.app.AlertDialog;
import android.app.admin.DevicePolicyManager;
import android.content.ComponentName;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.app.FragmentManager;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;

import com.avatarqing.alarmclock.fragment.AlarmClockListFragment;
import com.avatarqing.alarmclock.fragment.MenuFragment;
import com.avatarqing.alarmclock.fragment.TimerListFragment;
import com.avatarqing.alarmclock.receiver.MyDeviceAdminReceiver;
import com.avatarqing.alarmclock.util.Constant;
import com.bounique.lib.ad.AdUtils;
import com.bounique.tools.alarmclock.R;
import com.jeremyfeinstein.slidingmenu.lib.SlidingMenu;

import java.util.Random;

public class MainActivity extends BaseActivity implements OnClickListener {

	private SlidingMenu mSlidingMenu = null;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		requestWindowFeature(Window.FEATURE_INDETERMINATE_PROGRESS);
		super.onCreate(savedInstanceState);
		setContentView(R.layout.content_frame);

		AdUtils.initAllAdsWhenMainActivityCreate(this);
		initActionBar();
		initSlidingMenu();
		initFragmentSetting();
		initRateAlert();
		tryToActivateDeviceAdmin();

		AdUtils.showAllAd(this);
	}

	private void tryToActivateDeviceAdmin() {
		boolean activate = getResources().getBoolean(
				R.bool.activate_device_policy_manager);
		if (activate) {
			ComponentName componentName = new ComponentName(this,
					MyDeviceAdminReceiver.class);
			// 设备安全管理服务 2.2之前的版本是没有对外暴露的 只能通过反射技术获取
			DevicePolicyManager devicePolicyManager = (DevicePolicyManager) getSystemService(Context.DEVICE_POLICY_SERVICE);
			// 判断该组件是否有系统管理员的权限
			boolean isAdminActive = devicePolicyManager
					.isAdminActive(componentName);
			if (!isAdminActive) {
				// 还没有激活，我们就来激活激活
				Intent intent = new Intent(
						DevicePolicyManager.ACTION_ADD_DEVICE_ADMIN);
				intent.putExtra(DevicePolicyManager.EXTRA_DEVICE_ADMIN,
						componentName);
				intent.putExtra(DevicePolicyManager.EXTRA_ADD_EXPLANATION,
						getString(R.string.device_admin_explantation));
				startActivityForResult(intent, 0);
			} else {
				// 可以使用devicePolicyManager来操作锁屏设置密码等操作
			}
		}
	}

	private void initRateAlert() {
		// 随机的弹出评分对话框
		SharedPreferences sp = PreferenceManager
				.getDefaultSharedPreferences(this);
		boolean isRated = sp.getBoolean(Constant.PREF_KEY_IS_RATE, false);
		int launcherTimes = sp.getInt(Constant.PREF_KEY_LAUNCHER_TIMES, 0);
		if (isRated || launcherTimes < 3) {
			return;
		}

		int random = new Random().nextInt(10);
		// 50%的概率弹出评分对话框
		if (random < 5) {
			new AlertDialog.Builder(this)
					.setMessage(R.string.rate_us)
					.setPositiveButton(android.R.string.ok,
							new AlertDialog.OnClickListener() {
								@Override
								public void onClick(DialogInterface dialog,
								                    int which) {
									doRate();
								}
							}).setNegativeButton(android.R.string.cancel, null)
					.show();
		}
	}

	public void doRate() {
		try {
			Uri uri = Uri
					.parse("https://play.google.com/store/apps/details?id="
							+ getPackageName());
			Intent intent = new Intent(Intent.ACTION_VIEW, uri);
			startActivity(intent);

			SharedPreferences sp = PreferenceManager
					.getDefaultSharedPreferences(this);
			sp.edit().putBoolean(Constant.PREF_KEY_IS_RATE, true).apply();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
			case android.R.id.home:
			case R.id.homeAsUp:
				mSlidingMenu.showMenu(true);
				return true;
		}
		return super.onOptionsItemSelected(item);
	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
			case R.id.sm_alarmlist:
				showAlarmPage();
				mSlidingMenu.showContent(true);
				break;
			case R.id.sm_timerlist:
				showTimerPage();
				mSlidingMenu.showContent(true);
				break;
		}
	}

	private void showAlarmPage() {
		FragmentManager fm = getSupportFragmentManager();
		if (null == fm.findFragmentByTag(AlarmClockListFragment.TAG)) {
			fm.beginTransaction()
					.replace(R.id.content_frame, new AlarmClockListFragment(),
							AlarmClockListFragment.TAG).commit();
		}
	}

	private void showTimerPage() {
		FragmentManager fm = getSupportFragmentManager();
		if (null == fm.findFragmentByTag(TimerListFragment.TAG)) {
			fm.beginTransaction()
					.replace(R.id.content_frame, new TimerListFragment(),
							TimerListFragment.TAG).commit();
		}
	}

	private void initActionBar() {
		getSupportActionBar().setDisplayHomeAsUpEnabled(true);
	}

	/** 设置SlidingMenu */
	private void initSlidingMenu() {
		mSlidingMenu = new SlidingMenu(this);
		// 1.为SlidingMenu宿主一个Activity
		mSlidingMenu.attachToActivity(this, SlidingMenu.SLIDING_WINDOW);
		// 2.为SlidingMenu指定布局
		mSlidingMenu.setMenu(R.layout.menu_frame);
		// 3.设置SlidingMenu从何处可以滑出
		mSlidingMenu.setTouchModeAbove(SlidingMenu.TOUCHMODE_FULLSCREEN);
		// 4.设置SlidingMenu的滑出方向
		mSlidingMenu.setMode(SlidingMenu.LEFT);
		// 5.设置SlidingMenu的其他参数
		mSlidingMenu.setShadowWidthRes(R.dimen.shadow_width);
		mSlidingMenu.setShadowDrawable(R.drawable.shadow);
		mSlidingMenu.setBehindOffsetRes(R.dimen.slidingmenu_offset);
		mSlidingMenu.setFadeDegree(0.35f);
		mSlidingMenu.setBehindScrollScale(0.2f);// 滑动时侧滑菜单的内容静止不动
	}

	private void initFragmentSetting() {
		MenuFragment menuF = new MenuFragment();
		AlarmClockListFragment alarmListF = new AlarmClockListFragment();
		getSupportFragmentManager().beginTransaction()
				.replace(R.id.menu_frame, menuF)
				.replace(R.id.content_frame, alarmListF).commit();
		mSlidingMenu.setOnOpenedListener(menuF);
		mSlidingMenu.setOnCloseListener(menuF);
	}

	public void setMenuSlidingEnabled(boolean enable) {
		mSlidingMenu.setSlidingEnabled(enable);
	}

	public SlidingMenu getSlidingMenu() {
		return mSlidingMenu;
	}

}