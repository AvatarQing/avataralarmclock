/*
 * Copyright (C) 2009 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.avatarqing.alarmclock.activity;

import android.annotation.SuppressLint;
import android.media.AudioManager;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Bundle;
import android.preference.CheckBoxPreference;
import android.preference.ListPreference;
import android.preference.Preference;
import android.preference.PreferenceScreen;
import android.provider.Settings;
import android.view.MenuItem;

import com.avatarqing.alarmclock.preference.AlarmPreference;
import com.avatarqing.alarmclock.util.Constant;
import com.bounique.lib.ad.AdUtils;
import com.bounique.tools.alarmclock.R;
import com.umeng.analytics.MobclickAgent;

/**
 * Settings for the Alarm Clock.
 */
public class SettingsActivity extends BasePreferenceActivity implements
		Preference.OnPreferenceChangeListener {

	private static final int ALARM_STREAM_TYPE_BIT = 1 << AudioManager.STREAM_ALARM;

	private static final String KEY_ALARM_IN_SILENT_MODE = "alarm_in_silent_mode";
	public static final String KEY_ALARM_SNOOZE = "snooze_duration";
	public static final String KEY_VOLUME_BEHAVIOR = "volume_button_setting";
	public static final String KEY_DEFAULT_RINGTONE = "default_ringtone";
	public static final String KEY_AUTO_SILENCE = "auto_silence";
	public static final String KEY_ALARM_VIBRATE = "alarm_vibrate";

	@SuppressLint("NewApi")
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		addPreferencesFromResource(R.xml.settings);
//		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
//			getActionBar().setDisplayHomeAsUpEnabled(true);
//		}

		final AlarmPreference ringtone = (AlarmPreference) findPreference(KEY_DEFAULT_RINGTONE);
		Uri alert = RingtoneManager.getActualDefaultRingtoneUri(this,
				RingtoneManager.TYPE_ALARM);
		if (alert != null) {
			ringtone.setAlert(alert);
		}
		ringtone.setChangeDefault();
		AdUtils.showAllAd(this);
	}

	@Override
	protected void onResume() {
		super.onResume();
		refresh();
		MobclickAgent.onPageStart(Constant.PAGE_SETTING);
	}

	@Override
	protected void onPause() {
		super.onPause();
		MobclickAgent.onPageEnd(Constant.PAGE_SETTING);
	}

	@Override
	public boolean onPreferenceTreeClick(PreferenceScreen preferenceScreen,
	                                     Preference preference) {
		if (KEY_ALARM_IN_SILENT_MODE.equals(preference.getKey())) {
			CheckBoxPreference pref = (CheckBoxPreference) preference;
			int ringerModeStreamTypes = Settings.System.getInt(
					getContentResolver(),
					Settings.System.MODE_RINGER_STREAMS_AFFECTED, 0);

			if (pref.isChecked()) {
				ringerModeStreamTypes &= ~ALARM_STREAM_TYPE_BIT;
			} else {
				ringerModeStreamTypes |= ALARM_STREAM_TYPE_BIT;
			}

			Settings.System.putInt(getContentResolver(),
					Settings.System.MODE_RINGER_STREAMS_AFFECTED,
					ringerModeStreamTypes);

			return true;
		}

		return super.onPreferenceTreeClick(preferenceScreen, preference);
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
			case R.id.homeAsUp:
			case android.R.id.home:
				finish();
				overridePendingTransition(R.anim.close_enter, R.anim.close_exit);
				break;
		}
		return super.onOptionsItemSelected(item);
	}

	public boolean onPreferenceChange(Preference pref, Object newValue) {
		if (KEY_ALARM_SNOOZE.equals(pref.getKey())) {
			final ListPreference listPref = (ListPreference) pref;
			final int idx = listPref.findIndexOfValue((String) newValue);
			listPref.setSummary(listPref.getEntries()[idx]);
		} else if (KEY_AUTO_SILENCE.equals(pref.getKey())) {
			final ListPreference listPref = (ListPreference) pref;
			String delay = (String) newValue;
			updateAutoSnoozeSummary(listPref, delay);
		}
		return true;
	}

	private void updateAutoSnoozeSummary(ListPreference listPref, String delay) {
		int i = Integer.parseInt(delay);
		if (i == -1) {
			listPref.setSummary(R.string.auto_silence_never);
		} else {
			listPref.setSummary(getString(R.string.auto_silence_summary, i));
		}
	}

	private void refresh() {
		final CheckBoxPreference alarmInSilentModePref = (CheckBoxPreference) findPreference(KEY_ALARM_IN_SILENT_MODE);
		final int silentModeStreams = Settings.System.getInt(
				getContentResolver(),
				Settings.System.MODE_RINGER_STREAMS_AFFECTED, 0);
		alarmInSilentModePref
				.setChecked((silentModeStreams & ALARM_STREAM_TYPE_BIT) == 0);

		ListPreference listPref = (ListPreference) findPreference(KEY_ALARM_SNOOZE);
		listPref.setSummary(listPref.getEntry());
		listPref.setOnPreferenceChangeListener(this);

		listPref = (ListPreference) findPreference(KEY_AUTO_SILENCE);
		String delay = listPref.getValue();
		updateAutoSnoozeSummary(listPref, delay);
		listPref.setOnPreferenceChangeListener(this);
	}
}