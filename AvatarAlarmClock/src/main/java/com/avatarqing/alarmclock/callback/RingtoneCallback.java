package com.avatarqing.alarmclock.callback;
import android.net.Uri;

public interface RingtoneCallback {
	public void onRingtoneSelected(Uri uri);
}
