package com.avatarqing.alarmclock.receiver;

import android.app.admin.DeviceAdminReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

public class MyDeviceAdminReceiver extends DeviceAdminReceiver {
	private static final String TAG = MyDeviceAdminReceiver.class
			.getSimpleName();

	@Override
	public void onDisabled(Context context, Intent intent) {
		Log.i(TAG, "onDisabled");
		super.onDisabled(context, intent);
	}

	@Override
	public void onEnabled(Context context, Intent intent) {
		Log.i(TAG, "onEnabled");
		super.onEnabled(context, intent);
	}
}
