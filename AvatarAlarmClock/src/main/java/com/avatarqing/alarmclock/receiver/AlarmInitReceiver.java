package com.avatarqing.alarmclock.receiver;

import android.annotation.SuppressLint;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.PowerManager.WakeLock;

import com.avatarqing.alarmclock.entity.Alarm;
import com.avatarqing.alarmclock.util.AlarmAlertWakeLock;
import com.avatarqing.alarmclock.util.AlarmHelper;
import com.avatarqing.alarmclock.util.AsyncHandler;
import com.avatarqing.alarmclock.util.Log;
import com.avatarqing.alarmclock.util.TimerHelper;

public class AlarmInitReceiver extends BroadcastReceiver {
	private static final String TAG = "AlarmInitReceiver";

	/**
	 * Sets alarm on ACTION_BOOT_COMPLETED. Resets alarm on TIME_SET,
	 * TIMEZONE_CHANGED
	 */
	@SuppressLint("NewApi")
	@Override
	public void onReceive(final Context context, Intent intent) {
		final String action = intent.getAction();
		Log.v(TAG, "AlarmInitReceiver" + action);

		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
			final PendingResult result = goAsync();
			AsyncHandler.post(new Runnable() {
				@Override
				public void run() {
					handleIntent(context, action);
					result.finish();
				}
			});
		} else {
			handleIntent(context, action);
		}
	}

	private void handleIntent(Context context, String action) {
		final WakeLock wl = AlarmAlertWakeLock.createPartialWakeLock(context);
		wl.acquire();
		// Remove the snooze alarm after a boot.
		if (action.equals(Intent.ACTION_BOOT_COMPLETED)) {
			AlarmHelper.saveSnoozeAlert(context,
					Alarm.Columns.INVALID_ALARM_ID, -1);
		}

		AlarmHelper.disableExpiredAlarms(context);
		AlarmHelper.setNextAlert(context);
		TimerHelper.disableExpiredTimers(context);
		AlarmHelper.setNextAlert(context);
		
		wl.release();

		Log.v(TAG, "AlarmInitReceiver finished");
	}
}
