package com.avatarqing.alarmclock.receiver;

import android.annotation.SuppressLint;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Parcel;
import android.os.PowerManager.WakeLock;
import android.support.v4.app.NotificationCompat;

import com.avatarqing.alarmclock.activity.AlarmAlertFullScreen;
import com.avatarqing.alarmclock.entity.Alarm;
import com.avatarqing.alarmclock.entity.Timer;
import com.avatarqing.alarmclock.util.AlarmAlertWakeLock;
import com.avatarqing.alarmclock.util.AsyncHandler;
import com.avatarqing.alarmclock.util.Log;
import com.avatarqing.alarmclock.util.TimerHelper;
import com.bounique.tools.alarmclock.R;

public class TimerReceiver extends BroadcastReceiver {
	private static final String TAG = TimerReceiver.class.getSimpleName();

	/**
	 * If the alarm is older than STALE_WINDOW seconds, ignore. It is probably
	 * the result of a time or timezone change
	 */
	private final static int STALE_WINDOW = 60 * 30;

	@SuppressLint("NewApi")
	@Override
	public void onReceive(final Context context, final Intent intent) {
		final WakeLock wl = AlarmAlertWakeLock.createPartialWakeLock(context);
		wl.acquire();
		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
			final PendingResult result = goAsync();
			AsyncHandler.post(new Runnable() {
				@Override
				public void run() {
					handleIntent(context, intent);
					result.finish();
					wl.release();
				}
			});
		} else {
			handleIntent(context, intent);
			wl.release();
		}
	}

	public void handleIntent(Context context, Intent intent) {
		if (TimerHelper.TIMER_KILLED.equals(intent.getAction())) {
			// The alarm has been killed, update the notification
			Timer timer = (Timer) intent
					.getParcelableExtra(TimerHelper.TIMER_INTENT_EXTRA);
			Log.d(TAG, "TIMER_KILLED:" + "[id]:" + timer.id + "[label]:"
					+ timer.label);
			NotificationManager nm = getNotificationManager(context);
			nm.cancel(timer.getNotifiId());
			return;
		} else if (TimerHelper.CANCEL_DELAY_TIMER.equals(intent.getAction())) {
			Alarm alarm = null;
			if (intent.hasExtra(TimerHelper.TIMER_INTENT_EXTRA)) {
				// Get the alarm out of the Intent
				alarm = intent
						.getParcelableExtra(TimerHelper.TIMER_INTENT_EXTRA);
			}

			if (alarm != null) {
				TimerHelper.setNextAlert(context);
			}
			return;
		} else if (!TimerHelper.TIMER_ALERT_ACTION.equals(intent.getAction())) {
			// Unknown intent, bail.
			return;
		}

		Timer timer = null;
		// Grab the alarm from the intent. Since the remote AlarmManagerService
		// fills in the Intent to add some extra data, it must unparcel the
		// Alarm object. It throws a ClassNotFoundException when unparcelling.
		// To avoid this, do the marshalling ourselves.
		final byte[] data = intent
				.getByteArrayExtra(TimerHelper.TIMER_RAW_DATA);
		if (data != null) {
			Parcel in = Parcel.obtain();
			in.unmarshall(data, 0, data.length);
			in.setDataPosition(0);
			timer = Timer.CREATOR.createFromParcel(in);
		}

		if (timer == null) {
			Log.e(TAG, "Failed to parse the timer from the intent");
			// Make sure we set the next alert if needed.
			TimerHelper.setNextAlert(context);
			return;
		}

		// Intentionally verbose: always log the alarm time to provide useful
		// information in bug reports.
		long now = System.currentTimeMillis();
		Log.v(TAG, "Recevied timer [" + timer.label + "] set for "
				+ TimerHelper.formatTime(context, timer));

		// Always verbose to track down time change problems.
		long fireTime = TimerHelper.calculateAlarmTime(timer);
		if (now > fireTime + STALE_WINDOW) {
			Log.v(TAG, "Ignoring stale timer");
			return;
		}

		// Maintain a cpu wake lock until the AlarmAlert and AlarmKlaxon can
		// pick it up.
		AlarmAlertWakeLock.acquireCpuWakeLock(context);

		/* Close dialogs and window shade */
		Intent closeDialogs = new Intent(Intent.ACTION_CLOSE_SYSTEM_DIALOGS);
		context.sendBroadcast(closeDialogs);

		// Play the alarm alert and vibrate the device.
		Intent playAlarm = new Intent(TimerHelper.TIMER_ALERT_ACTION);
		playAlarm.addCategory(Intent.CATEGORY_DEFAULT);
		playAlarm.putExtra(TimerHelper.TIMER_INTENT_EXTRA, timer);
		context.startService(playAlarm);

		// Trigger a notification that, when clicked, will show the alarm alert
		// dialog. No need to check for fullscreen since this will always be
		// launched from a user action.
		Intent notify = new Intent(context, AlarmAlertFullScreen.class);
		notify.putExtra(TimerHelper.TIMER_INTENT_EXTRA, timer);
		PendingIntent pendingNotify = PendingIntent.getActivity(context,
				timer.id, notify, PendingIntent.FLAG_UPDATE_CURRENT);

		// Use the alarm's label or the default label as the ticker text and
		// main text of the notification.
		String label = timer.label;
		Notification n = new NotificationCompat.Builder(context)
				.setTicker(label).setWhen(fireTime)
				.setSmallIcon(R.drawable.stat_notify_alarm)
				.setContentTitle(label).setOngoing(true)
				.setContentText(context.getString(R.string.timer_notify_text))
				.setContentIntent(pendingNotify).build();

		n.flags |= Notification.FLAG_SHOW_LIGHTS;
		n.defaults |= Notification.DEFAULT_LIGHTS;

		// NEW: Embed the full-screen UI here. The notification manager will
		// take care of displaying it if it's OK to do so.
		Intent timerAlert = new Intent(context, AlarmAlertFullScreen.class);
		timerAlert.putExtra(TimerHelper.TIMER_INTENT_EXTRA, timer);
		timerAlert.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK
				| Intent.FLAG_ACTIVITY_NO_USER_ACTION);
		n.fullScreenIntent = PendingIntent.getActivity(context, timer.id,
				timerAlert, PendingIntent.FLAG_UPDATE_CURRENT);

		// Send the notification using the alarm id to easily identify the
		// correct notification.
		NotificationManager nm = getNotificationManager(context);
		nm.notify(timer.getNotifiId(), n);

		TimerHelper.deleteTimer(context, timer.id);
	}

	private NotificationManager getNotificationManager(Context context) {
		return (NotificationManager) context
				.getSystemService(Context.NOTIFICATION_SERVICE);
	}

}
