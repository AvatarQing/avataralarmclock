package com.avatarqing.alarmclock.widget;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Paint.Align;
import android.graphics.Paint.FontMetrics;
import android.graphics.Rect;
import android.graphics.drawable.BitmapDrawable;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;

import com.bounique.tools.alarmclock.R;

public class SlideButtonBar extends View {

	public interface Callback {
		public abstract void onBarEndReached();
	}

	private Bitmap mSlideButton = null;
	private int mMoveX = 0;
	private int mFingerInBtn = 0;
	private boolean mMoveValid = false;
	private Paint mPaint = null;
	private Rect mSrcRect = null;
	private Rect mDestRect = null;
	private float mTextBaseY = 0;

	private Callback mCallback = null;

	private String mLabel = null;

	private final double THRESHOLD = 0.75;

	public SlideButtonBar(Context context) {
		super(context);
		init();
	}

	public SlideButtonBar(Context context, AttributeSet attrs) {
		super(context, attrs);
		init();
	}

	@Override
	protected void onFinishInflate() {
		super.onFinishInflate();

		mMoveX = 0;
		mFingerInBtn = 0;
		mMoveValid = false;

		mSrcRect = new Rect();
		mDestRect = new Rect();

		mLabel = getContext().getString(R.string.alarm_alert_dismiss_text);

		mPaint = new Paint();
		mPaint.setColor(Color.DKGRAY);
		mPaint.setAntiAlias(true);
		mPaint.setTextAlign(Align.CENTER);

		mPaint.setTextSize(getResources().getDimension(R.dimen.draw_text_size));
	}

	@Override
	protected void onAttachedToWindow() {
		super.onAttachedToWindow();
		ViewGroup.LayoutParams lp = getLayoutParams();
		lp.height = mSlideButton.getHeight();
		setLayoutParams(lp);

		FontMetrics fontMetrics = mPaint.getFontMetrics();
		// 计算文字高度
		float fontHeight = fontMetrics.bottom - fontMetrics.top;
		// 计算文字baseline
		mTextBaseY = lp.height - (lp.height - fontHeight) / 2
				- fontMetrics.bottom;
	}

	@Override
	protected void onDraw(Canvas canvas) {
		super.onDraw(canvas);

		canvas.drawText(mLabel, getWidth() / 2, mTextBaseY, mPaint);

		// draw button
		int buttonWidth = mSlideButton.getWidth();
		int buttonHeight = mSlideButton.getHeight();
		int srcx = 0;
		int srcy = 0;
		int desx = mMoveX;
		int desy = 0;
		mSrcRect.set(srcx, srcy, srcx + buttonWidth, srcy + buttonHeight);
		mDestRect.set(desx, desy, desx + buttonWidth, desy + buttonHeight);
		canvas.drawBitmap(mSlideButton, mSrcRect, mDestRect, mPaint);

		postInvalidate();
	}

	/*
	 * The first method to count unlock is that in onTouchEvent. Another method
	 * is using GestureDetector to deal with it.
	 */
	@Override
	public boolean onTouchEvent(MotionEvent event) {
		// get touched position
		int x = (int) event.getX();
		int y = (int) event.getY();

		switch (event.getAction()) {
			case MotionEvent.ACTION_MOVE:

				if (mMoveValid) {
					x -= mFingerInBtn;

					// if((x < this.mBgImage.getWidth() - LEFT_MARGIN + MOVE_X_PIXEL
					// - this.mSlideBtnImage.getWidth())
					// && (x > LEFT_MARGIN))
					// movex = (int)event.getX() - fingerInBtn;

					if (isButtonReachedEnd()) {
						// 滑块滑到头了
						mMoveX = this.getWidth() - this.mSlideButton.getWidth();
					} else if (x < 0) {
						mMoveX = 0;
					} else {
						mMoveX = (int) event.getX() - mFingerInBtn;
					}
				}

				break;
			case MotionEvent.ACTION_DOWN:

				int topY = 0;
				int bottomY = topY + mSlideButton.getHeight();

				if ((y > topY) && (y < bottomY) && (x > mMoveX)
						&& (x < mMoveX + mSlideButton.getWidth())) {
					// 按到了滑块
					mFingerInBtn = (int) event.getX() - mMoveX;
					mMoveValid = true;
				} else {
					mMoveValid = false;
				}

				break;
			case MotionEvent.ACTION_UP:
				mMoveValid = false;
				int widthLength = this.getWidth() - mSlideButton.getWidth();
				if (mMoveX * 1.0 / widthLength < THRESHOLD) {
					// 没滑到终点，返回到初始位置
					mMoveX = 0;
				} else {
					// 快滑动终点，就当做是滑动终点了
					mMoveX = this.getWidth() - this.mSlideButton.getWidth();
				}
				// judge unlock or not
				if (isButtonReachedEnd()) {
					if (mCallback != null) {
						mCallback.onBarEndReached();
					}
				}
				break;
		}

		// redraw with new param
		this.postInvalidate();

		super.onTouchEvent(event);
		return true;
	}

	private void init() {
		mSlideButton = ((BitmapDrawable) this.getResources().getDrawable(
				R.drawable.slide_thumb)).getBitmap();
	}

	public void setCallback(Callback callback) {
		this.mCallback = callback;
	}

	private boolean isButtonReachedEnd() {
		if (mMoveX >= getWidth() - mSlideButton.getWidth()) {
			return true;
		} else {
			return false;
		}
	}
}