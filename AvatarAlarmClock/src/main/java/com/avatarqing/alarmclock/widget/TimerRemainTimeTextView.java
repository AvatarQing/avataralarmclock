package com.avatarqing.alarmclock.widget;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Handler;
import android.os.SystemClock;
import android.util.AttributeSet;
import android.widget.TextView;

import com.avatarqing.alarmclock.util.Constant;
import com.avatarqing.alarmclock.util.TimerHelper;
import com.bounique.tools.alarmclock.R;

public class TimerRemainTimeTextView extends TextView {

	private Handler mHandler = new Handler();;
	private boolean mAttached = false;
	private boolean mTimerOn = true;
	private long mStartTime = -1;
	private int mHours = 0;
	private int mMinutes = 0;
	private int mSeconds = 0;

	public TimerRemainTimeTextView(Context context) {
		super(context);
	}

	public TimerRemainTimeTextView(Context context, AttributeSet attrs) {
		super(context, attrs);
	}

	@Override
	protected void onAttachedToWindow() {
		super.onAttachedToWindow();

		if (!mAttached) {
			mAttached = true;
			IntentFilter filter = new IntentFilter();

			filter.addAction(Intent.ACTION_TIME_CHANGED);
			filter.addAction(Intent.ACTION_TIMEZONE_CHANGED);

			getContext().registerReceiver(mIntentReceiver, filter, null,
					mHandler);

			mHandler.removeCallbacks(mRefreshTask);
			mHandler.post(mRefreshTask);
		}
	}

	@Override
	protected void onDetachedFromWindow() {
		super.onDetachedFromWindow();
		if (mAttached) {
			getContext().unregisterReceiver(mIntentReceiver);
			mAttached = false;
		}
	}

	public void setTimeArgs(long timeInMillis, int destHours, int destMinutes,
	                        int destSeconds) {
		mStartTime = timeInMillis;
		mHours = destHours;
		mMinutes = destMinutes;
		mSeconds = destSeconds;
		mHandler.removeCallbacks(mRefreshTask);
		mHandler.post(mRefreshTask);
	}

	public void setTimerOn(boolean isOn) {
		mTimerOn = isOn;
	}

	private void onTimeChanged() {
		String timeStr = formatTime(mStartTime);
		if (timeStr != null && mTimerOn) {
			// 还没到时间，显示剩下多少时间
			setText(timeStr);
		} else {
			// 时间已经过头了
			setText(R.string.disabled);
		}
	}

	/**
	 * 将给定的毫秒数转化为日、时、分的格式
	 */
	private String formatTime(long timeInMillis) {
		long remainTime = TimerHelper.calculateRemainTime(mHours, mMinutes,
				mSeconds, mStartTime);
		if (remainTime > 0) {
			long hours = remainTime / (1000 * 60 * 60);
			long minutes = remainTime / (1000 * 60) % 60;
			long seconds = remainTime / 1000 % 60;
			hours = hours % 24;
			return String.format(Constant.FORMAT_TIME, hours, minutes, seconds);
		} else {
			return null;
		}
	}

	private Runnable mRefreshTask = new Runnable() {

		@Override
		public void run() {
			onTimeChanged();
			invalidate();

			long now = SystemClock.uptimeMillis();
			long next = now + (1000 - now % 1000);
			mHandler.postAtTime(mRefreshTask, next);
		}
	};

	private final BroadcastReceiver mIntentReceiver = new BroadcastReceiver() {
		@Override
		public void onReceive(Context context, Intent intent) {
			onTimeChanged();
			invalidate();
		}
	};
}