package com.avatarqing.alarmclock.widget;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.util.AttributeSet;
import android.widget.TextView;

import com.bounique.tools.alarmclock.R;

public class AlarmRemainTimeTextView extends TextView {

	private Context mContext;
	private boolean mAttached = false;
	private boolean mAlarmOn = true;
	private long mDestTime = -1;

	public AlarmRemainTimeTextView(Context context) {
		super(context);
		init(context);
	}

	public AlarmRemainTimeTextView(Context context, AttributeSet attrs) {
		super(context, attrs);
		init(context);
	}

	private void init(Context context) {
		this.mContext = context;
	}

	@Override
	protected void onAttachedToWindow() {
		super.onAttachedToWindow();

		if (!mAttached) {
			mAttached = true;
			IntentFilter filter = new IntentFilter();

			filter.addAction(Intent.ACTION_TIME_TICK);
			filter.addAction(Intent.ACTION_TIME_CHANGED);
			filter.addAction(Intent.ACTION_TIMEZONE_CHANGED);

			getContext().registerReceiver(mIntentReceiver, filter);
		}
	}

	@Override
	protected void onDetachedFromWindow() {
		super.onDetachedFromWindow();
		if (mAttached) {
			getContext().unregisterReceiver(mIntentReceiver);
			mAttached = false;
		}
	}

	public void setDestTime(long timeInMillis) {
		mDestTime = timeInMillis;
		onTimeChanged();
		invalidate();
	}

	public void setAlarmOn(boolean isOn) {
		mAlarmOn = isOn;
	}

	private void onTimeChanged() {
		String timeStr = formatTime(mDestTime);
		if (timeStr != null && mAlarmOn) {
			// 还没到时间，显示剩下多少时间
			setText(timeStr);
		} else {
			// 时间已经过头了，提示闹钟未开启
			setText(R.string.disabled);
		}
	}

	/**
	 * 将给定的毫秒数转化为日、时、分的格式
	 */
	private String formatTime(long timeInMillis) {
		long delta = timeInMillis - System.currentTimeMillis();
		if (delta < 0) {
			return null;
		}
		long hours = delta / (1000 * 60 * 60);
		long minutes = delta / (1000 * 60) % 60;
		long days = hours / 24;
		hours = hours % 24;

		String daySeq = (days == 0) ? "" : (days == 1) ? mContext
				.getString(R.string.day) : mContext.getString(R.string.days,
				Long.toString(days));

		String minSeq = (minutes == 0) ? "" : (minutes == 1) ? mContext
				.getString(R.string.minute) : mContext.getString(
				R.string.minutes, Long.toString(minutes));

		String hourSeq = (hours == 0) ? "" : (hours == 1) ? mContext
				.getString(R.string.hour) : mContext.getString(R.string.hours,
				Long.toString(hours));

		boolean dispDays = days > 0;
		boolean dispHour = hours > 0;
		boolean dispMinute = minutes > 0;

		int index = (dispDays ? 1 : 0) | (dispHour ? 2 : 0)
				| (dispMinute ? 4 : 0);

		String[] formats = mContext.getResources().getStringArray(
				R.array.alarm_set_tick);
		return String.format(formats[index], daySeq, hourSeq, minSeq);
	}

	private final BroadcastReceiver mIntentReceiver = new BroadcastReceiver() {
		@Override
		public void onReceive(Context context, Intent intent) {
			onTimeChanged();
			invalidate();
		}
	};
}