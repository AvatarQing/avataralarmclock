package com.avatarqing.alarmclock.fragment;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.v4.app.LoaderManager.LoaderCallbacks;
import android.support.v4.content.CursorLoader;
import android.support.v4.content.Loader;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.FrameLayout;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.SimpleCursorAdapter;
import android.widget.TextView;

import com.avatarqing.alarmclock.callback.RingtoneCallback;
import com.avatarqing.alarmclock.util.AudioPlayer;
import com.avatarqing.alarmclock.util.Constant;
import com.avatarqing.alarmclock.util.Log;
import com.avatarqing.alarmclock.util.StorageUtil;
import com.bounique.tools.alarmclock.R;
import com.umeng.analytics.MobclickAgent;

public class SetCustomRingtoneFragment extends BaseFragment implements
		OnItemClickListener {
	public static final String TAG = "SetCustomRingtoneFragment";

	private final int LOADER_ID_CUSTOM_BELL = 1;

	private FrameLayout mContainer = null;
	private ListView mCustomRingList = null;
	private ProgressBar mProgressBar = null;
	private TextView mTextHint = null;

	private SimpleCursorAdapter mCustomCursorAdapter = null;

	private Uri mRingtone = null;

	private RingtoneCallback mCallbacks = null;
	private RingtoneCallback mDummyCallbacks = new RingtoneCallback() {
		@Override
		public void onRingtoneSelected(Uri uri) {
		}
	};

	@Override
	public void onAttach(Activity activity) {
		Log.i(TAG, "onAttach");
		super.onAttach(activity);
		if (!(activity instanceof RingtoneCallback)) {
			throw new ClassCastException(
					"Host activity must implement SetRingtoneFragment.Callbacks");
		}
		mCallbacks = (RingtoneCallback) activity;

		Bundle args = getArguments();
		if (args != null) {
			mRingtone = args.getParcelable(Constant.ARG_RINGTONE);
		}
		startWatchingExternalStorage();
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
	                         Bundle savedInstanceState) {
		Log.i(TAG, "onCreateView");
		FrameLayout.LayoutParams lp = new FrameLayout.LayoutParams(
				LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT,
				Gravity.CENTER);

		mContainer = new FrameLayout(getActivity());
		mTextHint = new TextView(getActivity());
		mCustomRingList = new ListView(getActivity());
		mProgressBar = new ProgressBar(getActivity());
		mProgressBar.setLayoutParams(lp);
		mTextHint.setText(R.string.external_storage_unavailable);
		mTextHint.setLayoutParams(lp);
		mTextHint.setTextAppearance(getActivity(),
				android.R.attr.textAppearanceLarge);
		mContainer.addView(mProgressBar);

		return mContainer;
	}

	@Override
	public void onViewCreated(View view, Bundle savedInstanceState) {
		Log.i(TAG, "onViewCreated");
		super.onViewCreated(view, savedInstanceState);

		mCustomRingList.setChoiceMode(ListView.CHOICE_MODE_SINGLE);
		mCustomRingList.setFastScrollEnabled(true);
		mCustomRingList.setOnItemClickListener(SetCustomRingtoneFragment.this);
	}

	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);

		if (StorageUtil.isExternalStorageAvaliable()) {
			getLoaderManager().restartLoader(LOADER_ID_CUSTOM_BELL, null,
					mLoaderCallbacks);
		} else {
			mContainer.removeAllViews();
			mContainer.addView(mTextHint);
		}
	}

	@Override
	public void onResume() {
		Log.i(TAG, "onResume");
		super.onResume();

		MobclickAgent.onPageStart(Constant.PAGE_SET_CUSTOM_RINGTONE);
	}

	@Override
	public void onPause() {
		Log.i(TAG, "onPause");
		super.onPause();

		MobclickAgent.onPageEnd(Constant.PAGE_SET_CUSTOM_RINGTONE);
	}

	@Override
	public void onStop() {
		Log.i(TAG, "onStop");
		super.onStop();

		AudioPlayer.getInstance().processStopRequest();
	}

	@Override
	public void onDetach() {
		Log.i(TAG, "onDetach");
		super.onDetach();
		mCallbacks = mDummyCallbacks;
		getActivity().unregisterReceiver(mExternalStorageReceiver);
	};

	@Override
	public void onItemClick(AdapterView<?> parent, View view, int position,
	                        long id) {
		if (parent == mCustomRingList) {// 点击的是外存上的铃声列表
			int columnIndex = mCustomCursorAdapter.getCursor().getColumnIndex(
					MediaStore.Audio.Media.DATA);
			String filePath = ((Cursor) mCustomCursorAdapter.getItem(position))
					.getString(columnIndex);
			Uri uri = Uri.parse(filePath);

			mCallbacks.onRingtoneSelected(uri);

			togglePlay(uri);
		}
	}

	private void togglePlay(Uri uri) {
		Uri playingUri = AudioPlayer.getInstance().getPlayingUri();
		if (playingUri != null && playingUri.equals(uri)) {
			// 点击的是当前正在播放(暂停)的歌曲，就暂停(播放)
			int state = AudioPlayer.getInstance().getState();
			switch (state) {
				case AudioPlayer.STATE_PLAYING:
					AudioPlayer.getInstance().processPauseRequest();
					break;
				case AudioPlayer.STATE_PAUSED:
					AudioPlayer.getInstance().processPlayRequest(uri);
					break;
			}
		} else {
			AudioPlayer.getInstance().processStopRequest();
			AudioPlayer.getInstance().processPlayRequest(uri);
		}
	}

	/** 选中已读入的外存音乐条目 */
	private void initCustomRingtoneSelect() {
		if (mRingtone == null) {
			return;
		}

		Cursor cursor = mCustomCursorAdapter.getCursor();
		if (cursor != null && cursor.moveToFirst()) {
			int indexPath = cursor.getColumnIndex(MediaStore.MediaColumns.DATA);
			int position = 0;
			String path;
			do {
				path = cursor.getString(indexPath);
				if (Uri.parse(path).equals(mRingtone)) {
					mCustomRingList.setItemChecked(position, true);
					break;
				}
				position++;
			} while (cursor.moveToNext());
		}

	}

	private void startWatchingExternalStorage() {
		IntentFilter intentFilter = new IntentFilter();
		intentFilter.addAction(Intent.ACTION_MEDIA_MOUNTED);
		intentFilter.addAction(Intent.ACTION_MEDIA_REMOVED);
		intentFilter.addAction(Intent.ACTION_MEDIA_BAD_REMOVAL);
		intentFilter.addAction(Intent.ACTION_MEDIA_EJECT);
		intentFilter.setPriority(Integer.MAX_VALUE);
		intentFilter.addDataScheme("file");
		getActivity().registerReceiver(mExternalStorageReceiver, intentFilter);
	}

	private BroadcastReceiver mExternalStorageReceiver = new BroadcastReceiver() {
		@Override
		public void onReceive(Context context, Intent intent) {
			if (intent.getAction().equals(Intent.ACTION_MEDIA_EJECT)
					|| intent.getAction().equals(Intent.ACTION_MEDIA_REMOVED)
					|| intent.getAction().equals(
					Intent.ACTION_MEDIA_BAD_REMOVAL)) {
				// SD卡移除
				mCustomCursorAdapter.changeCursor(null);
				mContainer.removeAllViews();
				mContainer.addView(mTextHint);
			} else if (intent.getAction().equals(Intent.ACTION_MEDIA_MOUNTED)) {
				// SD卡正常挂载
				getLoaderManager().restartLoader(LOADER_ID_CUSTOM_BELL, null,
						mLoaderCallbacks);
			}
		}
	};

	private LoaderCallbacks<Cursor> mLoaderCallbacks = new LoaderCallbacks<Cursor>() {

		@Override
		public void onLoaderReset(Loader<Cursor> loader) {

		}

		@SuppressWarnings("deprecation")
		@Override
		public void onLoadFinished(Loader<Cursor> loader, Cursor data) {
			mContainer.removeAllViews();
			mContainer.addView(mCustomRingList);
			mCustomCursorAdapter = new SimpleCursorAdapter(getActivity(),
					R.layout.simple_list_item_single_choice, data,
					new String[] { MediaStore.Audio.Media.TITLE },
					new int[] { android.R.id.text1 });
			mCustomRingList.setAdapter(mCustomCursorAdapter);
			initCustomRingtoneSelect();
		}

		@Override
		public Loader<Cursor> onCreateLoader(int id, Bundle args) {
			// TODO 弄个加载进度
			String[] projection = new String[] { MediaStore.Audio.Media._ID,
					MediaStore.Audio.Media.TITLE, MediaStore.Audio.Media.DATA };
			return new CursorLoader(getActivity(),
					MediaStore.Audio.Media.EXTERNAL_CONTENT_URI, projection,
					null, null, MediaStore.Audio.Media.DEFAULT_SORT_ORDER);
		}
	};

}