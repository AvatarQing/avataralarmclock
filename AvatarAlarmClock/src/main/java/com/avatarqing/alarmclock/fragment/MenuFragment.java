package com.avatarqing.alarmclock.fragment;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.animation.AnimationUtils;
import android.widget.TextView;

import com.avatarqing.alarmclock.activity.AboutUsActivity;
import com.avatarqing.alarmclock.activity.SettingsActivity;
import com.avatarqing.alarmclock.util.Constant;
import com.avatarqing.alarmclock.util.Log;
import com.bounique.tools.alarmclock.R;
import com.jeremyfeinstein.slidingmenu.lib.SlidingMenu.OnCloseListener;
import com.jeremyfeinstein.slidingmenu.lib.SlidingMenu.OnOpenedListener;
import com.umeng.analytics.MobclickAgent;

import java.util.Calendar;

public class MenuFragment extends BaseFragment implements OnCloseListener,
		OnOpenedListener {
	private static final String TAG = "MenuFragment";

	private View mBtnNewAlarm = null;
	private View mBtnNewTimer = null;
	private View mBtnSetting = null;
	private View mBtnInfo = null;
	private TextView mGreet = null;

	private OnClickListener mHostClickListener = null;

	@Override
	public void onAttach(Activity activity) {
		Log.i(TAG, "onAttach");
		if (!(activity instanceof OnClickListener)) {
			throw new ClassCastException("Host activity must implement "
					+ OnClickListener.class.getName());
		}
		mHostClickListener = (OnClickListener) activity;
		super.onAttach(activity);
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
	                         Bundle savedInstanceState) {
		Log.i(TAG, "onCreateView");
		View rootView = inflater
				.inflate(R.layout.slidingmenu, container, false);
		return rootView;
	}

	@Override
	public void onViewCreated(View view, Bundle savedInstanceState) {
		Log.i(TAG, "onViewCreated");
		super.onViewCreated(view, savedInstanceState);
		mGreet = (TextView) view.findViewById(R.id.tv_greet);
		mBtnNewAlarm = view.findViewById(R.id.sm_alarmlist);
		mBtnNewTimer = view.findViewById(R.id.sm_timerlist);
		mBtnSetting = view.findViewById(R.id.sm_setting);
		mBtnInfo = view.findViewById(R.id.sm_info);

		mBtnNewAlarm.setOnClickListener(mOnClickListener);
		mBtnNewTimer.setOnClickListener(mOnClickListener);
		mBtnSetting.setOnClickListener(mOnClickListener);
		mBtnInfo.setOnClickListener(mOnClickListener);
	}

	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		Log.i(TAG, "onActivityCreated");
		super.onActivityCreated(savedInstanceState);
		setTimeGreet();
	}

	@Override
	public void onResume() {
		Log.i(TAG, "onResume");
		super.onResume();
		MobclickAgent.onPageStart(Constant.PAGE_SLIDING_MENU_LEFT);
	}

	@Override
	public void onPause() {
		Log.i(TAG, "onPause");
		super.onPause();
		MobclickAgent.onPageEnd(Constant.PAGE_SLIDING_MENU_LEFT);
	}

	@Override
	public void onDetach() {
		Log.i(TAG, "onDetach");
		super.onDetach();
	}

	@Override
	public void onOpened() {
		if (mGreet.getVisibility() == View.VISIBLE) {
			// 侧滑菜单还没关闭，那就不用再重放了
			return;
		}
		mGreet.setVisibility(View.VISIBLE);
		mGreet.startAnimation(AnimationUtils.loadAnimation(getActivity(),
				R.anim.fade_in));
	}

	@Override
	public void onClose() {
		mGreet.startAnimation(AnimationUtils.loadAnimation(getActivity(),
				android.R.anim.fade_out));
		mGreet.setVisibility(View.INVISIBLE);
	}

	private void setTimeGreet() {
		Calendar calendar = Calendar.getInstance();
		int hour = calendar.get(Calendar.HOUR_OF_DAY);
		if (hour >= 5 && hour < 11) {
			mGreet.setText(R.string.good_morning);
		} else if (hour >= 11 && hour < 13) {
			mGreet.setText(R.string.good_noon);
		} else if (hour >= 13 && hour < 18) {
			mGreet.setText(R.string.good_afternoon);
		} else if (hour >= 18 && hour < 24 || hour >= 0 && hour < 5) {
			mGreet.setText(R.string.good_evening);
		}
	}

	private View.OnClickListener mOnClickListener = new View.OnClickListener() {

		@Override
		public void onClick(View v) {
			switch (v.getId()) {
				case R.id.sm_alarmlist:
					mHostClickListener.onClick(v);
					break;
				case R.id.sm_timerlist:
					mHostClickListener.onClick(v);
					break;
				case R.id.sm_setting:
					startActivity(new Intent(getActivity(), SettingsActivity.class));
					getActivity().overridePendingTransition(R.anim.open_enter,
							R.anim.open_exit);
					break;
				case R.id.sm_info:
					startActivity(new Intent(getActivity(), AboutUsActivity.class));
					getActivity().overridePendingTransition(R.anim.open_enter,
							R.anim.open_exit);
					break;
			}
		}
	};

}