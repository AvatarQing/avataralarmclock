package com.avatarqing.alarmclock.fragment;

import net.simonvt.timepicker.TimePicker;
import net.simonvt.timepicker.TimePicker.OnTimeChangedListener;
import android.app.Activity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;

import com.avatarqing.alarmclock.util.AlarmHelper;
import com.avatarqing.alarmclock.util.Constant;
import com.bounique.tools.alarmclock.R;
import com.umeng.analytics.MobclickAgent;

public class SetTimeFragment extends BaseFragment {
	public static final String TAG = "SetTimeFragment";

	private EditText mLabel = null;
	private TimePicker mTimePicker = null;

	private TextWatcher mTextWatcher = null;
	private OnTimeChangedListener mOnTimeChangedListener = null;

	public void onAttach(Activity activity) {
		Log.i(TAG, "onAttach");
		super.onAttach(activity);

		if (!(activity instanceof OnTimeChangedListener)) {
			throw new ClassCastException(
					"Host activity must implement OnTimeChangedListener.");
		}
		if (!(activity instanceof TextWatcher)) {
			throw new ClassCastException(
					"Host activity must implement TextWatcher.");
		}
		mTextWatcher = (TextWatcher) activity;
		mOnTimeChangedListener = (OnTimeChangedListener) activity;

	};

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		Log.i(TAG, "onCreateView");
		return inflater.inflate(R.layout.set_time, container, false);
	}

	@Override
	public void onViewCreated(View view, Bundle savedInstanceState) {
		Log.i(TAG, "onViewCreated");
		super.onViewCreated(view, savedInstanceState);
		mLabel = (EditText) view.findViewById(R.id.alarm_label);
		mTimePicker = (TimePicker) view.findViewById(R.id.alarm_timePicker);

		mLabel.addTextChangedListener(mTextWatcher);
		mTimePicker.setOnTimeChangedListener(mOnTimeChangedListener);
		boolean is24Hour = AlarmHelper.get24HourMode(getActivity());
		mTimePicker.setIs24HourView(is24Hour);
		mTimePicker.enableSecondPick(false);

		handleArgument();
	}

	@Override
	public void onResume() {
		Log.i(TAG, "onResume");
		super.onResume();

		MobclickAgent.onPageStart(Constant.PAGE_SET_TIME);
	}

	@Override
	public void onPause() {
		Log.i(TAG, "onPause");
		super.onPause();

		MobclickAgent.onPageEnd(Constant.PAGE_SET_TIME);
	}

	private void handleArgument() {
		Bundle args = getArguments();
		int hour = args.getInt(Constant.ARG_HOUR);
		int minute = args.getInt(Constant.ARG_MINUTE);
		int second = args.getInt(Constant.ARG_SECOND);
		int mode = args.getInt(Constant.ARG_TIMEPICK_MODE);
		boolean secondEnabled = args
				.getBoolean(Constant.ARG_TIMEPICK_SECOND_ENABLED);
		String label = args.getString(Constant.ARG_LABEL);
		mTimePicker.enableSecondPick(secondEnabled);
		mTimePicker.setMode(mode);
		mTimePicker.setCurrentHour(hour);
		mTimePicker.setCurrentMinute(minute);
		mTimePicker.setCurrentSecond(second);

		mLabel.setText(label);
	}

	@Override
	public void onDetach() {
		Log.i(TAG, "onDetach");
		super.onDetach();
		mOnTimeChangedListener = mDummyOnTimeChangedListener;
		mTextWatcher = mDummyTextWatcher;
	}

	private OnTimeChangedListener mDummyOnTimeChangedListener = new OnTimeChangedListener() {
		@Override
		public void onTimeChanged(TimePicker view, int hourOfDay, int minute,
				int second) {

		}
	};
	private TextWatcher mDummyTextWatcher = new TextWatcher() {

		@Override
		public void onTextChanged(CharSequence s, int start, int before,
				int count) {

		}

		@Override
		public void beforeTextChanged(CharSequence s, int start, int count,
				int after) {

		}

		@Override
		public void afterTextChanged(Editable s) {

		}
	};
}
