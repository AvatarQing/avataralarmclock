package com.avatarqing.alarmclock.fragment;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.database.DataSetObserver;
import android.os.Bundle;
import android.os.Vibrator;
import android.support.v4.app.LoaderManager.LoaderCallbacks;
import android.support.v4.content.CursorLoader;
import android.support.v4.content.Loader;
import android.support.v7.view.ActionMode;
import android.util.SparseBooleanArray;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.AdapterView.OnItemLongClickListener;
import android.widget.ListView;
import android.widget.TextView;

import com.avatarqing.alarmclock.activity.MainActivity;
import com.avatarqing.alarmclock.activity.SetTimerActivity;
import com.avatarqing.alarmclock.adapter.TimerListAdapter;
import com.avatarqing.alarmclock.entity.Timer;
import com.avatarqing.alarmclock.util.Constant;
import com.avatarqing.alarmclock.util.Log;
import com.avatarqing.alarmclock.util.TimerHelper;
import com.avatarqing.alarmclock.widget.BounceListView;
import com.bounique.tools.alarmclock.R;
import com.umeng.analytics.MobclickAgent;

public class TimerListFragment extends BaseFragment implements OnClickListener {
	public static final String TAG = TimerListFragment.class.getSimpleName();

	private BounceListView mListView = null;
	private View mEmptyView = null;
	private TextView mBtnNewAlarm = null;
	private TextView mBtnDeleteAlarm = null;
	private ViewGroup mBottomBar = null;
	private TimerListAdapter mAdapter = null;
	private final int LOADER_ID_TIMERLIST = 1;
	private MainActivity mActivity = null;

	private Vibrator mVibrator = null;

	private int mLastLongClickedPos = -1;
	private ActionMode mMode;

	@Override
	public void onAttach(Activity activity) {
		Log.i(TAG, "onAttach");
		super.onAttach(activity);
		setHasOptionsMenu(true);

		if (activity instanceof MainActivity) {
			mActivity = (MainActivity) activity;
		}
		mVibrator = (Vibrator) activity
				.getSystemService(Context.VIBRATOR_SERVICE);
		if (getActionBarActivity().getSupportActionBar() != null) {
			getActionBarActivity().getSupportActionBar().setTitle(
					R.string.timer);
		}
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
	                         Bundle savedInstanceState) {
		Log.i(TAG, "onCreateView");
		View rootView = inflater.inflate(R.layout.timer_list, container, false);
		return rootView;
	}

	@Override
	public void onViewCreated(View view, Bundle savedInstanceState) {
		Log.i(TAG, "onViewCreated");
		super.onViewCreated(view, savedInstanceState);
		mBtnNewAlarm = (TextView) view.findViewById(R.id.btn_new_timer);
		mBtnDeleteAlarm = (TextView) view.findViewById(R.id.btn_delete_timer);
		mListView = (BounceListView) view.findViewById(R.id.list_timer);
		mEmptyView = view.findViewById(R.id.emptyview_timer_list);
		mBottomBar = (ViewGroup) view.findViewById(R.id.timerlist_bottom_bar);
		mAdapter = new TimerListAdapter(getActivity(), mListView, null, 0);
		mListView.setAdapter(mAdapter);
		mListView.setOnItemClickListener(mOnItemClickListener);
		mListView.setOnItemLongClickListener(mOnItemLongClickListener);
		mBtnNewAlarm.setOnClickListener(this);
		mBtnDeleteAlarm.setOnClickListener(this);
	}

	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		Log.i(TAG, "onActivityCreated");
		super.onActivityCreated(savedInstanceState);
	}

	@Override
	public void onResume() {
		Log.i(TAG, "onResume");
		super.onResume();

		// 加载闹钟列表
		getLoaderManager().restartLoader(LOADER_ID_TIMERLIST, null,
				mLoaderCallbacks);
		mListView.startLayoutAnimation();
		mBottomBar.startLayoutAnimation();

		MobclickAgent.onPageStart(Constant.PAGE_TIMER_LIST);
	}

	@Override
	public void onPause() {
		Log.i(TAG, "onPause");
		super.onPause();

		MobclickAgent.onPageEnd(Constant.PAGE_TIMER_LIST);
	}

	@Override
	public void onStop() {
		Log.i(TAG, "onStop");
		super.onStop();

	}

	public void onDetach() {
		Log.i(TAG, "onDetach");
		super.onDetach();
		mActivity = null;
	};

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
			case R.id.btn_new_timer:
				mVibrator.vibrate(100);
				startActivity(new Intent(getActivity(), SetTimerActivity.class));
				getActivity().overridePendingTransition(R.anim.open_enter,
						R.anim.open_exit);
				break;
			case R.id.btn_delete_timer:
				deleteSelectedAlarms();
				break;
		}
	}

	private void deleteSelectedAlarms() {
		final long[] checkedIds = mListView.getCheckedItemIds();
		if (checkedIds != null && checkedIds.length != 0) {
			new AlertDialog.Builder(getActivity())
					.setTitle(getString(R.string.delete_timer))
					.setMessage(getString(R.string.delete_timer_confirm))
					.setPositiveButton(android.R.string.ok,
							new DialogInterface.OnClickListener() {
								public void onClick(DialogInterface d, int w) {
									// 批量删除闹钟
									for (long id : checkedIds) {
										TimerHelper.deleteTimer(getActivity(),
												(int) id);
									}

									mListView.clearChoices();
									mAdapter.notifyDataSetChanged();
									mMode.finish();
								}
							}).setNegativeButton(android.R.string.cancel, null)
					.show();
		}
	}

	private LoaderCallbacks<Cursor> mLoaderCallbacks = new LoaderCallbacks<Cursor>() {
		@Override
		public Loader<Cursor> onCreateLoader(int id, Bundle args) {
			mListView.setVisibility(View.GONE);
			mListView.setEmptyView(null);
			getActionBarActivity()
					.setSupportProgressBarIndeterminateVisibility(true);
			return new CursorLoader(getActivity(), Timer.Columns.CONTENT_URI,
					Timer.Columns.TIMER_QUERY_COLUMNS, null, null,
					Timer.Columns.DEFAULT_SORT_ORDER);
		}

		@Override
		public void onLoaderReset(Loader<Cursor> loader) {
			mAdapter.swapCursor(null);
		}

		@Override
		public void onLoadFinished(Loader<Cursor> loader, Cursor data) {
			mListView.setVisibility(View.VISIBLE);
			mAdapter.swapCursor(data);
			mListView.setEmptyView(mEmptyView);
			getActionBarActivity()
					.setSupportProgressBarIndeterminateVisibility(false);
		}
	};
	private OnItemClickListener mOnItemClickListener = new OnItemClickListener() {

		@Override
		public void onItemClick(AdapterView<?> parent, View view, int position,
		                        long id) {
			// 开启定时器后，屏蔽点击

			// final Cursor c = (Cursor) mAdapter.getItem(position);
			// final Timer timer = new Timer(c);
			// Intent intent = new Intent(getActivity(),
			// SetTimerActivity.class);
			// intent.putExtra(TimerHelper.TIMER_INTENT_EXTRA, timer);
			// startActivity(intent);
			// getActivity().overridePendingTransition(R.anim.open_enter,
			// R.anim.open_exit);
		}
	};
	private OnItemLongClickListener mOnItemLongClickListener = new OnItemLongClickListener() {

		@Override
		public boolean onItemLongClick(AdapterView<?> parent, View view,
		                               int position, long id) {
			mLastLongClickedPos = position;
			getActionBarActivity().startSupportActionMode(mActionModeCallback);
			return true;
		}
	};

	private ActionMode.Callback mActionModeCallback = new ActionMode.Callback() {

		@Override
		public boolean onPrepareActionMode(ActionMode mode, Menu menu) {
			return false;
		}

		@Override
		public void onDestroyActionMode(ActionMode mode) {
			mMode = null;
			if (mActivity != null) {
				mActivity.setMenuSlidingEnabled(true);
			}

			mListView.setChoiceMode(ListView.CHOICE_MODE_NONE);
			mListView.setOnItemClickListener(mOnItemClickListener);
			mAdapter.notifyDataSetChanged();
			mAdapter.unregisterDataSetObserver(mDataSetObserver);

			mBtnDeleteAlarm.setVisibility(View.GONE);
			mBtnNewAlarm.setVisibility(View.VISIBLE);
		}

		@Override
		public boolean onCreateActionMode(ActionMode mode, Menu menu) {
			mMode = mode;

			// TODO 动画切换图标
			mBtnDeleteAlarm.setVisibility(View.VISIBLE);
			mBtnNewAlarm.setVisibility(View.GONE);

			if (mActivity != null) {
				mActivity.setMenuSlidingEnabled(false);
			}

			mListView.setChoiceMode(ListView.CHOICE_MODE_MULTIPLE);
			mListView.setOnItemClickListener(mContextItemClickListener);
			mListView.clearChoices();
			mAdapter.notifyDataSetChanged();

			mAdapter.registerDataSetObserver(mDataSetObserver);
			mListView.setItemChecked(mLastLongClickedPos, true);
			mMode.setTitle(getCheckedItemCount() + "/" + mAdapter.getCount());
			return true;
		}

		@Override
		public boolean onActionItemClicked(ActionMode mode, MenuItem item) {
			return false;
		}

		private int getCheckedItemCount() {
			int count = 0;
			SparseBooleanArray checked = mListView.getCheckedItemPositions();
			if (checked != null) {
				for (int i = 0; i < checked.size(); i++) {
					if (checked.valueAt(i)) {
						count++;
					}
				}
			}
			return count;
		}

		private DataSetObserver mDataSetObserver = new DataSetObserver() {
			@Override
			public void onChanged() {
				super.onChanged();
				if (mMode != null) {
					mMode.setTitle(getCheckedItemCount() + "/"
							+ mAdapter.getCount());
				}
			}
		};

		private OnItemClickListener mContextItemClickListener = new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> parent, View view,
			                        int position, long id) {
				boolean oldValue = !mListView.isItemChecked(position);
				mListView.setItemChecked(position, !oldValue);
				mAdapter.notifyDataSetChanged();
				mMode.setTitle(getCheckedItemCount() + "/"
						+ mAdapter.getCount());
			}
		};
	};
}