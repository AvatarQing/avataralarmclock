package com.avatarqing.alarmclock.fragment;

import android.app.Activity;
import android.database.Cursor;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.CheckedTextView;
import android.widget.ListView;
import android.widget.SimpleCursorAdapter;

import com.avatarqing.alarmclock.callback.RingtoneCallback;
import com.avatarqing.alarmclock.util.AudioPlayer;
import com.avatarqing.alarmclock.util.Constant;
import com.avatarqing.alarmclock.util.Log;
import com.bounique.tools.alarmclock.R;
import com.umeng.analytics.MobclickAgent;

public class SetSystemRingtoneFragment extends BaseFragment implements
		OnItemClickListener {
	public static final String TAG = "SetSystemRingtoneFragment";

	private RingtoneManager mRingtoneManager = null;

	private ListView mSystemRingList = null;

	private SimpleCursorAdapter mSystemCursorAdapter = null;

	private Uri mRingtone = null;

	private RingtoneCallback mCallbacks = null;
	private RingtoneCallback mDummyCallbacks = new RingtoneCallback() {
		@Override
		public void onRingtoneSelected(Uri uri) {
		}
	};

	@Override
	public void onAttach(Activity activity) {
		Log.i(TAG, "onAttach");
		super.onAttach(activity);
		if (!(activity instanceof RingtoneCallback)) {
			throw new ClassCastException(
					"Host activity must implement SetRingtoneFragment.Callbacks");
		}
		mCallbacks = (RingtoneCallback) activity;
		mRingtoneManager = new RingtoneManager(activity);

		Bundle args = getArguments();
		if (args != null) {
			mRingtone = args.getParcelable(Constant.ARG_RINGTONE);
		}
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
	                         Bundle savedInstanceState) {
		Log.i(TAG, "onCreateView");
		mSystemRingList = new ListView(getActivity());
		return mSystemRingList;
	}

	@Override
	public void onViewCreated(View view, Bundle savedInstanceState) {
		Log.i(TAG, "onViewCreated");
		super.onViewCreated(view, savedInstanceState);

		mSystemRingList.setChoiceMode(ListView.CHOICE_MODE_SINGLE);
		mSystemRingList.setFastScrollEnabled(true);
		mSystemRingList.setOnItemClickListener(SetSystemRingtoneFragment.this);
	}

	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		Log.i(TAG, "onActivityCreated");
		super.onActivityCreated(savedInstanceState);

		addSilentItem();
		loadSystemRingtones();
		initSystemRingtoneSelect();
	}

	@Override
	public void onResume() {
		Log.i(TAG, "onResume");
		super.onResume();

		MobclickAgent.onPageStart(Constant.PAGE_SET_SYSTEM_RINGTONE);
	}

	@Override
	public void onPause() {
		Log.i(TAG, "onPause");
		super.onPause();

		MobclickAgent.onPageEnd(Constant.PAGE_SET_SYSTEM_RINGTONE);
	}

	@Override
	public void onStop() {
		Log.i(TAG, "onStop");
		super.onStop();

		AudioPlayer.getInstance().processStopRequest();
	}

	@Override
	public void onDetach() {
		Log.i(TAG, "onDetach");
		super.onDetach();
		mCallbacks = mDummyCallbacks;
	};

	@Override
	public void onItemClick(AdapterView<?> parent, View view, int position,
	                        long id) {
		int headerCount = mSystemRingList.getHeaderViewsCount();
		if (position < headerCount) {
			// 选择了静音
			mCallbacks.onRingtoneSelected(null);
		} else {
			int realPosition = ListView.INVALID_POSITION;
			realPosition = position - headerCount;
			Uri uri = mRingtoneManager.getRingtoneUri(realPosition);
			mCallbacks.onRingtoneSelected(uri);

			togglePlay(uri);
		}
	}

	private void togglePlay(Uri uri) {
		Uri playingUri = AudioPlayer.getInstance().getPlayingUri();
		if (uri.equals(playingUri)) {
			// 点击的是当前正在播放(暂停)的歌曲，就暂停(播放)
			int state = AudioPlayer.getInstance().getState();
			switch (state) {
				case AudioPlayer.STATE_PLAYING:
					AudioPlayer.getInstance().processPauseRequest();
					break;
				case AudioPlayer.STATE_PAUSED:
					AudioPlayer.getInstance().processPlayRequest(uri);
					break;
			}
		} else {
			AudioPlayer.getInstance().processStopRequest();
			AudioPlayer.getInstance().processPlayRequest(uri);
		}
	}

	/** 在系统铃声列表顶部添加“静音”选项 */
	private void addSilentItem() {
		CheckedTextView headerView = (CheckedTextView) LayoutInflater.from(
				getActivity()).inflate(R.layout.simple_list_item_single_choice,
				null);
		headerView.setText(getString(R.string.silent));
		headerView.setMinHeight(getResources().getDimensionPixelSize(
				R.dimen.listview_min_height));
		mSystemRingList.addHeaderView(headerView);
	}

	/** 加载系统铃声 */
	@SuppressWarnings("deprecation")
	private void loadSystemRingtones() {
		mRingtoneManager.setType(RingtoneManager.TYPE_ALARM);
		Cursor cursor = mRingtoneManager.getCursor();
		mSystemCursorAdapter = new SimpleCursorAdapter(getActivity(),
				R.layout.simple_list_item_single_choice, cursor,
				new String[] { cursor
						.getColumnName(RingtoneManager.TITLE_COLUMN_INDEX) },
				new int[] { android.R.id.text1 });
		mSystemRingList.setAdapter(mSystemCursorAdapter);
	}

	/** 选中已读入的系统铃声条目 */
	private void initSystemRingtoneSelect() {
		if (mRingtone == null) {
			// 选中静音项
			mSystemRingList.setItemChecked(0, true);
		} else {
			int position = mRingtoneManager.getRingtonePosition(mRingtone);
			if (position != -1) {
				mSystemRingList.setItemChecked(
						position + mSystemRingList.getHeaderViewsCount(), true);
			}
		}
	}

}