package com.avatarqing.alarmclock.fragment;

import android.support.v4.app.Fragment;
import android.support.v7.app.ActionBarActivity;

public class BaseFragment extends Fragment {

	public ActionBarActivity getActionBarActivity() {
		return (ActionBarActivity) getActivity();
	}

}
