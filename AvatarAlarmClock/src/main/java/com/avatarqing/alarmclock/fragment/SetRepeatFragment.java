package com.avatarqing.alarmclock.fragment;

import android.app.Activity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import com.avatarqing.alarmclock.entity.Alarm;
import com.avatarqing.alarmclock.entity.Alarm.DaysOfWeek;
import com.avatarqing.alarmclock.util.Constant;
import com.avatarqing.alarmclock.util.Log;
import com.bounique.tools.alarmclock.R;
import com.umeng.analytics.MobclickAgent;

import java.text.DateFormatSymbols;
import java.util.Calendar;

public class SetRepeatFragment extends BaseFragment implements
		OnItemClickListener {
	public interface Callbacks {
		public void onRepeatSetChanged(Alarm.DaysOfWeek newDaysOfWeek);
	}

	public static final String TAG = "SetRepeatFragment";

	private String[] mEntries = null;

	private ListView mListView = null;

	private ArrayAdapter<String> mAdpater = null;
	private Alarm.DaysOfWeek mDaysOfWeek = null;

	private Callbacks mCallback = null;

	@Override
	public void onAttach(Activity activity) {
		Log.i(TAG, "onAttach");
		super.onAttach(activity);

		if (!(activity instanceof Callbacks)) {
			throw new ClassCastException(
					"Host activity must implement SetRepeatFragment.Callbacks");
		}

		mCallback = (Callbacks) activity;

		String[] weekdays = new DateFormatSymbols().getWeekdays();
		mEntries = new String[] { weekdays[Calendar.MONDAY],
				weekdays[Calendar.TUESDAY], weekdays[Calendar.WEDNESDAY],
				weekdays[Calendar.THURSDAY], weekdays[Calendar.FRIDAY],
				weekdays[Calendar.SATURDAY], weekdays[Calendar.SUNDAY] };
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
	                         Bundle savedInstanceState) {
		Log.i(TAG, "onCreateView");
		return inflater.inflate(R.layout.set_repeat, container, false);
	}

	@Override
	public void onViewCreated(View view, Bundle savedInstanceState) {
		Log.i(TAG, "onViewCreated");
		super.onViewCreated(view, savedInstanceState);
		initViewSetting(view);
	}

	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		Log.i(TAG, "onActivityCreated");
		super.onActivityCreated(savedInstanceState);

		// 初始化数据
		mDaysOfWeek = getArguments().getParcelable(Constant.ARG_DAYSOFWEEK);
		if (mDaysOfWeek == null) {
			mDaysOfWeek = new Alarm.DaysOfWeek(0);
		}
		boolean[] checks = mDaysOfWeek.getBooleanArray();
		for (int i = 0; i < checks.length; i++) {
			mListView.setItemChecked(i, checks[i]);
		}
	}

	@Override
	public void onResume() {
		Log.i(TAG, "onResume");
		super.onResume();

		MobclickAgent.onPageStart(Constant.PAGE_SET_REPEAT);
	}

	@Override
	public void onPause() {
		Log.i(TAG, "onPause");
		super.onPause();

		MobclickAgent.onPageEnd(Constant.PAGE_SET_REPEAT);
	}

	@Override
	public void onDetach() {
		Log.i(TAG, "onDetach");
		super.onDetach();
		mCallback = mDummyCallback;
	}

	@Override
	public void onItemClick(AdapterView<?> parent, View view, int position,
	                        long id) {
		// isItemChecked()取值时真实状态的反值，不解
		mDaysOfWeek.set(position, mListView.isItemChecked(position));
		mCallback.onRepeatSetChanged(mDaysOfWeek);
	}

	private void initViewSetting(View rootView) {
		mListView = (ListView) rootView.findViewById(R.id.list_repeat);

		mListView.setChoiceMode(ListView.CHOICE_MODE_MULTIPLE);
		mAdpater = new ArrayAdapter<String>(getActivity(),
				R.layout.simple_list_item_multiple_choice, android.R.id.text1,
				mEntries);
		mListView.setAdapter(mAdpater);
		mListView.setOnItemClickListener(SetRepeatFragment.this);

	}

	private Callbacks mDummyCallback = new Callbacks() {
		@Override
		public void onRepeatSetChanged(DaysOfWeek newDaysOfWeek) {

		}
	};
}