/*
 * Copyright (C) 2009 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.avatarqing.alarmclock.entity;

import android.database.Cursor;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Parcel;
import android.os.Parcelable;
import android.provider.BaseColumns;

import com.avatarqing.alarmclock.provider.AlarmProvider;
import com.avatarqing.alarmclock.util.AlarmHelper;
import com.avatarqing.alarmclock.util.Log;

public final class Timer implements Parcelable {
	private static final String TAG = "Timer";
	// ////////////////////////////
	// Parcelable apis
	// ////////////////////////////
	public static final Parcelable.Creator<Timer> CREATOR = new Parcelable.Creator<Timer>() {
		public Timer createFromParcel(Parcel p) {
			return new Timer(p);
		}

		public Timer[] newArray(int size) {
			return new Timer[size];
		}
	};

	public int describeContents() {
		return 0;
	}

	public void writeToParcel(Parcel p, int flags) {
		p.writeInt(id);
		p.writeInt(enabled ? 1 : 0);
		p.writeInt(hour);
		p.writeInt(minutes);
		p.writeInt(second);
		p.writeLong(startTime);
		p.writeInt(vibrate ? 1 : 0);
		p.writeString(label);
		p.writeParcelable(alert, flags);
		p.writeInt(silent ? 1 : 0);
	}

	// ////////////////////////////
	// end Parcelable apis
	// ////////////////////////////

	// ////////////////////////////
	// Column definitions
	// ////////////////////////////
	public static class Columns implements BaseColumns {
		/**
		 * The content:// style URL for this table
		 */
		public static final Uri CONTENT_URI = Uri.parse("content://"
				+ AlarmProvider.AUTHORITIES + "/" + AlarmProvider.PATH_TIMER);

		/**
		 * Hour in 24-hour localtime 0 - 23.
		 * <P>
		 * Type: INTEGER
		 * </P>
		 */
		public static final String HOUR = "hour";

		/**
		 * Minutes in localtime 0 - 59
		 * <P>
		 * Type: INTEGER
		 * </P>
		 */
		public static final String MINUTES = "minutes";
		/**
		 * SECOND in localtime 0 - 59
		 * <P>
		 * Type: INTEGER
		 * </P>
		 */
		public static final String SECOND = "second";

		/**
		 * start time of countdown in UTC milliseconds from the epoch.
		 * <P>
		 * Type: INTEGER
		 * </P>
		 */
		public static final String START_TIME = "starttime";
		/**
		 * True if alarm is active
		 * <P>
		 * Type: BOOLEAN
		 * </P>
		 */
		public static final String ENABLED = "enabled";

		/**
		 * True if alarm should vibrate
		 * <P>
		 * Type: BOOLEAN
		 * </P>
		 */
		public static final String VIBRATE = "vibrate";

		/**
		 * Message to show when alarm triggers Note: not currently used
		 * <P>
		 * Type: STRING
		 * </P>
		 */
		public static final String MESSAGE = "message";

		/**
		 * Audio alert to play when alarm triggers
		 * <P>
		 * Type: STRING
		 * </P>
		 */
		public static final String ALERT = "alert";

		/**
		 * The default sort order for this table
		 */
		public static final String DEFAULT_SORT_ORDER = HOUR + ", " + MINUTES
				+ ", " + SECOND + ", " + START_TIME + " ASC";

		// Used when filtering enabled alarms.
		public static final String WHERE_ENABLED = ENABLED + "=1";

		public static final String[] TIMER_QUERY_COLUMNS = { _ID, HOUR,
				MINUTES, SECOND, START_TIME, ENABLED, VIBRATE, MESSAGE, ALERT };

		/**
		 * These save calls to cursor.getColumnIndexOrThrow() THEY MUST BE KEPT
		 * IN SYNC WITH ABOVE QUERY COLUMNS
		 */
		public static final int TIMER_ID_INDEX = 0;
		public static final int TIMER_HOUR_INDEX = 1;
		public static final int TIMER_MINUTE_INDEX = 2;
		public static final int TIMER_SECOND_INDEX = 3;
		public static final int TIMER_START_TIME_INDEX = 4;
		public static final int TIMER_ENABLED_INDEX = 5;
		public static final int TIMER_VIBRATE_INDEX = 6;
		public static final int TIMER_MESSAGE_INDEX = 7;
		public static final int TIMER_ALERT_INDEX = 8;

		public static final int INVALID_TIMER_ID = -1;
	}

	// ////////////////////////////
	// End column definitions
	// ////////////////////////////

	// Public fields
	public int id;
	public boolean enabled;
	public int hour;
	public int minutes;
	public int second;
	public long startTime;
	public boolean vibrate;
	public String label;
	public Uri alert;
	public boolean silent;

	public Timer(Cursor c) {
		id = c.getInt(Columns.TIMER_ID_INDEX);
		enabled = c.getInt(Columns.TIMER_ENABLED_INDEX) == 1;
		hour = c.getInt(Columns.TIMER_HOUR_INDEX);
		minutes = c.getInt(Columns.TIMER_MINUTE_INDEX);
		second = c.getInt(Columns.TIMER_SECOND_INDEX);
		startTime = c.getLong(Columns.TIMER_START_TIME_INDEX);
		vibrate = c.getInt(Columns.TIMER_VIBRATE_INDEX) == 1;
		label = c.getString(Columns.TIMER_MESSAGE_INDEX);

		String alertString = c.getString(Columns.TIMER_ALERT_INDEX);
		if (AlarmHelper.ALARM_ALERT_SILENT.equals(alertString)) {
			Log.v(TAG, "Alarm is marked as silent");
			silent = true;
		} else {
			if (alertString != null && alertString.length() != 0) {
				alert = Uri.parse(alertString);
			}

			// If the database alert is null or it failed to parse, use the
			// default alert.
			if (alert == null) {
				alert = RingtoneManager
						.getDefaultUri(RingtoneManager.TYPE_ALARM);
			}
		}
	}

	public Timer(Parcel p) {
		id = p.readInt();
		enabled = p.readInt() == 1;
		hour = p.readInt();
		minutes = p.readInt();
		second = p.readInt();
		startTime = p.readLong();
		vibrate = p.readInt() == 1;
		label = p.readString();
		alert = (Uri) p.readParcelable(null);
		silent = p.readInt() == 1;
	}

	// Creates a default alarm at the current time.
	public Timer() {
		id = Columns.INVALID_TIMER_ID;
		enabled = false;
		hour = 0;
		minutes = 0;
		second = 1;
		startTime = 0;
		vibrate = true;
		alert = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_ALARM);
		silent = false;
		label = null;
	}

	public int getNotifiId() {
		// XXX 为了避免和Alarm的Notification id冲突，临时的解决方案
		return Integer.MAX_VALUE / 2 + id;
	}

	@Override
	public int hashCode() {
		return id;
	}

	@Override
	public boolean equals(Object o) {
		if (!(o instanceof Timer))
			return false;
		final Timer other = (Timer) o;
		return id == other.id;
	}
}