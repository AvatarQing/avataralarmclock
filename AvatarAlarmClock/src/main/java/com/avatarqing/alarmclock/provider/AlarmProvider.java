/*
 * Copyright (C) 2007 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.avatarqing.alarmclock.provider;

import android.content.ContentProvider;
import android.content.ContentUris;
import android.content.ContentValues;
import android.content.Context;
import android.content.UriMatcher;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.database.sqlite.SQLiteQueryBuilder;
import android.net.Uri;
import android.text.TextUtils;

import com.avatarqing.alarmclock.entity.Alarm;
import com.avatarqing.alarmclock.entity.Timer;
import com.avatarqing.alarmclock.util.Log;

public class AlarmProvider extends ContentProvider {
	private static final String TAG = "AlarmProvider";
	public static final String AUTHORITIES = "com.joymeng.alarmclock";
	public static final String PATH_ALARM = "alarm";
	public static final String PATH_TIMER = "timer";

	private SQLiteOpenHelper mOpenHelper;

	private static final String TABLE_ALARMS = "alarms";
	private static final String TABLE_TIMERS = "timers";
	private static final int ALARMS = 1;
	private static final int ALARMS_ID = 2;
	private static final int TIMERS = 3;
	private static final int TIMERS_ID = 4;
	private static final UriMatcher sURLMatcher = new UriMatcher(
			UriMatcher.NO_MATCH);

	static {
		sURLMatcher.addURI(AUTHORITIES, PATH_ALARM, ALARMS);
		sURLMatcher.addURI(AUTHORITIES, PATH_ALARM + "/#", ALARMS_ID);
		sURLMatcher.addURI(AUTHORITIES, PATH_TIMER, TIMERS);
		sURLMatcher.addURI(AUTHORITIES, PATH_TIMER + "/#", TIMERS_ID);
	}

	public AlarmProvider() {
	}

	@Override
	public boolean onCreate() {
		mOpenHelper = new DatabaseHelper(getContext());
		return true;
	}

	@Override
	public Cursor query(Uri url, String[] projectionIn, String selection,
			String[] selectionArgs, String sort) {
		SQLiteQueryBuilder qb = new SQLiteQueryBuilder();

		// Generate the body of the query
		int match = sURLMatcher.match(url);
		switch (match) {
		case ALARMS:
			qb.setTables(TABLE_ALARMS);
			break;
		case ALARMS_ID:
			qb.setTables(TABLE_ALARMS);
			qb.appendWhere("_id=");
			qb.appendWhere(url.getPathSegments().get(1));
			break;
		case TIMERS:
			qb.setTables(TABLE_TIMERS);
			break;
		case TIMERS_ID:
			qb.setTables(TABLE_TIMERS);
			qb.appendWhere("_id=");
			qb.appendWhere(url.getPathSegments().get(1));
			break;
		default:
			throw new IllegalArgumentException("Unknown URL " + url);
		}

		SQLiteDatabase db = mOpenHelper.getReadableDatabase();
		Cursor ret = qb.query(db, projectionIn, selection, selectionArgs, null,
				null, sort);

		if (ret == null) {
			Log.v(TAG, "query: failed");
		} else {
			ret.setNotificationUri(getContext().getContentResolver(), url);
		}

		return ret;
	}

	@Override
	public String getType(Uri url) {
		int match = sURLMatcher.match(url);
		switch (match) {
		case ALARMS:
			return "vnd.android.cursor.dir/" + TABLE_ALARMS;
		case ALARMS_ID:
			return "vnd.android.cursor.item/" + TABLE_ALARMS;
		case TIMERS:
			return "vnd.android.cursor.dir/" + TABLE_TIMERS;
		case TIMERS_ID:
			return "vnd.android.cursor.item/" + TABLE_TIMERS;
		default:
			throw new IllegalArgumentException("Unknown URL");
		}
	}

	@Override
	public int update(Uri url, ContentValues values, String where,
			String[] whereArgs) {
		int count;
		long rowId = 0;
		int match = sURLMatcher.match(url);
		SQLiteDatabase db = mOpenHelper.getWritableDatabase();
		switch (match) {
		case ALARMS_ID: {
			String segment = url.getPathSegments().get(1);
			rowId = Long.parseLong(segment);
			count = db.update(TABLE_ALARMS, values, "_id=" + rowId, null);
			break;
		}
		case TIMERS_ID: {
			String segment = url.getPathSegments().get(1);
			rowId = Long.parseLong(segment);
			count = db.update(TABLE_TIMERS, values, "_id=" + rowId, null);
			break;
		}
		default: {
			throw new UnsupportedOperationException("Cannot update URL: " + url);
		}
		}
		Log.v(TAG, "*** notifyChange() rowId: " + rowId + " url " + url);
		getContext().getContentResolver().notifyChange(url, null);
		return count;
	}

	@Override
	public Uri insert(Uri url, ContentValues initialValues) {
		int match = sURLMatcher.match(url);
		if (match != ALARMS && match != TIMERS) {
			throw new IllegalArgumentException("Cannot insert into URL: " + url);
		}
		ContentValues values = new ContentValues(initialValues);
		SQLiteDatabase db = mOpenHelper.getWritableDatabase();

		Uri newUrl = null;
		switch (match) {
		case ALARMS:
			long alarmRowId = db.insert(TABLE_ALARMS, Alarm.Columns.MESSAGE,
					values);
			if (alarmRowId < 0) {
				throw new SQLException("Failed to insert row into " + url);
			}
			Log.v(TAG, "Added alarm rowId = " + alarmRowId);
			newUrl = ContentUris.withAppendedId(Alarm.Columns.CONTENT_URI,
					alarmRowId);
			break;
		case TIMERS:
			long timeRowId = db.insert(TABLE_TIMERS, Timer.Columns.MESSAGE,
					values);
			if (timeRowId < 0) {
				throw new SQLException("Failed to insert row into " + url);
			}
			Log.v(TAG, "Added timer rowId = " + timeRowId);
			newUrl = ContentUris.withAppendedId(Alarm.Columns.CONTENT_URI,
					timeRowId);
			break;
		}

		getContext().getContentResolver().notifyChange(newUrl, null);
		return newUrl;
	}

	public int delete(Uri url, String where, String[] whereArgs) {
		SQLiteDatabase db = mOpenHelper.getWritableDatabase();
		int count;
		long rowId = 0;
		switch (sURLMatcher.match(url)) {
		case ALARMS: {
			count = db.delete(TABLE_ALARMS, where, whereArgs);
			break;
		}
		case ALARMS_ID: {
			String segment = url.getPathSegments().get(1);
			rowId = Long.parseLong(segment);
			if (TextUtils.isEmpty(where)) {
				where = "_id=" + segment;
			} else {
				where = "_id=" + segment + " AND (" + where + ")";
			}
			count = db.delete(TABLE_ALARMS, where, whereArgs);
			Log.v(TAG, "deleted alarm rowId = " + rowId);
			break;
		}
		case TIMERS: {
			count = db.delete(TABLE_TIMERS, where, whereArgs);
			break;
		}
		case TIMERS_ID: {
			String segment = url.getPathSegments().get(1);
			rowId = Long.parseLong(segment);
			if (TextUtils.isEmpty(where)) {
				where = "_id=" + segment;
			} else {
				where = "_id=" + segment + " AND (" + where + ")";
			}
			count = db.delete(TABLE_TIMERS, where, whereArgs);
			Log.v(TAG, "deleted timer rowId = " + rowId);
			break;
		}
		default:
			throw new IllegalArgumentException("Cannot delete from URL: " + url);
		}

		getContext().getContentResolver().notifyChange(url, null);
		return count;
	}

	private static class DatabaseHelper extends SQLiteOpenHelper {
		private static final String DATABASE_NAME = "alarms.db";
		private static final int DATABASE_VERSION = 5;

		public DatabaseHelper(Context context) {
			super(context, DATABASE_NAME, null, DATABASE_VERSION);
		}

		@Override
		public void onCreate(SQLiteDatabase db) {
			db.execSQL("CREATE TABLE " + TABLE_ALARMS + " ("
					+ "_id INTEGER PRIMARY KEY," + Alarm.Columns.HOUR
					+ " INTEGER, " + Alarm.Columns.MINUTES + " INTEGER, "
					+ Alarm.Columns.DAYS_OF_WEEK + " INTEGER, "
					+ Alarm.Columns.ALARM_TIME + " INTEGER, "
					+ Alarm.Columns.ENABLED + " INTEGER, "
					+ Alarm.Columns.VIBRATE + " INTEGER, "
					+ Alarm.Columns.MESSAGE + " TEXT, " + Alarm.Columns.ALERT
					+ " TEXT);");

			db.execSQL("CREATE TABLE " + TABLE_TIMERS + " ("
					+ "_id INTEGER PRIMARY KEY," + Timer.Columns.HOUR
					+ " INTEGER, " + Timer.Columns.MINUTES + " INTEGER, "
					+ Timer.Columns.SECOND + " INTEGER, "
					+ Timer.Columns.START_TIME + " INTEGER, "
					+ Timer.Columns.ENABLED + " INTEGER, "
					+ Timer.Columns.VIBRATE + " INTEGER, "
					+ Timer.Columns.MESSAGE + " TEXT, " + Timer.Columns.ALERT
					+ " TEXT);");

			// insert default alarms
			// String insertMe = "INSERT INTO alarms " + "(" +
			// Alarm.Columns.HOUR
			// + ", " + Alarm.Columns.MINUTES + ", "
			// + Alarm.Columns.DAYS_OF_WEEK + ", "
			// + Alarm.Columns.ALARM_TIME + ", " + Alarm.Columns.ENABLED
			// + ", " + Alarm.Columns.VIBRATE + ", "
			// + Alarm.Columns.MESSAGE + ", " + Alarm.Columns.ALERT + ") "
			// + "VALUES ";
			// db.execSQL(insertMe + "(8, 30, 31, 0, 0, 1, '', '');");
			// db.execSQL(insertMe + "(9, 00, 96, 0, 0, 1, '', '');");
		}

		@Override
		public void onUpgrade(SQLiteDatabase db, int oldVersion,
				int currentVersion) {
			Log.v(TAG, "Upgrading alarms database from version " + oldVersion
					+ " to " + currentVersion
					+ ", which will destroy all old data");
			db.execSQL("DROP TABLE IF EXISTS " + TABLE_ALARMS);
			db.execSQL("DROP TABLE IF EXISTS " + TABLE_TIMERS);
			onCreate(db);
		}
	}

}
