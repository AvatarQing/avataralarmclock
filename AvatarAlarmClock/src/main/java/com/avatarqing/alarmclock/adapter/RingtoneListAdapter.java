package com.avatarqing.alarmclock.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.bounique.tools.alarmclock.R;

public class RingtoneListAdapter extends BaseAdapter {
	private int[] iconResIds = { R.drawable.icon_record_by_self,
			R.drawable.icon_system_ring, R.drawable.icon_external_ring };
	private String[] array = null;
	private Context mContext = null;

	public RingtoneListAdapter(Context context) {
		mContext = context;
		array = context.getResources().getStringArray(R.array.choose_bell);
	}

	@Override
	public int getCount() {
		return iconResIds.length;
	}

	@Override
	public String getItem(int position) {
		if (position >= 0 && position < getCount()) {
			return array[position];
		} else {
			return null;
		}
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		ViewHolder holder = null;
		if (convertView == null) {
			holder = new ViewHolder();
			convertView = LayoutInflater.from(mContext).inflate(
					R.layout.simple_list_item, null);
			holder.iv = (ImageView) convertView.findViewById(R.id.icon);
			holder.tv = (TextView) convertView.findViewById(android.R.id.text1);
			convertView.setTag(holder);
		} else {
			holder = (ViewHolder) convertView.getTag();
		}
		holder.tv.setText(array[position]);
		holder.iv.setImageResource(iconResIds[position]);
		return convertView;
	}

	private class ViewHolder {
		public TextView tv;
		public ImageView iv;
	}
}
