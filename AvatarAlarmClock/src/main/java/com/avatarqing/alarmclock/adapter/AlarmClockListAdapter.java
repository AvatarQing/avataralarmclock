package com.avatarqing.alarmclock.adapter;

import java.util.Locale;

import android.content.Context;
import android.database.Cursor;
import android.support.v4.widget.CursorAdapter;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ListView;
import android.widget.TextView;

import com.avatarqing.alarmclock.activity.SetAlarmActivity;
import com.avatarqing.alarmclock.entity.Alarm;
import com.avatarqing.alarmclock.util.AlarmHelper;
import com.avatarqing.alarmclock.widget.AlarmRemainTimeTextView;
import com.avatarqing.alarmclock.widget.AndroidClockTextView;
import com.avatarqing.alarmclock.widget.SwitchButton;
import com.avatarqing.alarmclock.widget.SwitchButton.OnMyCheckedChangeListener;
import com.bounique.tools.alarmclock.R;

public class AlarmClockListAdapter extends CursorAdapter implements
		OnMyCheckedChangeListener {
	private static final String TAG = "AlarmClockListAdapter";
	private Context mContext = null;
	private ListView mListView = null;

	public AlarmClockListAdapter(Context context, ListView listview,
			Cursor cursor, int flag) {
		super(context, cursor, flag);
		mContext = context;
		mListView = listview;
	}

	@Override
	public View newView(Context context, Cursor cursor, ViewGroup parent) {
		View rootView = LayoutInflater.from(context).inflate(
				R.layout.item_alarmclock, parent, false);
		return rootView;
	}

	@Override
	public void bindView(View view, Context context, Cursor cursor) {
		final Alarm alarm = new Alarm(cursor);
		ViewHolder holder = null;
		if (view.getTag() == null) {
			holder = new ViewHolder();
			holder.alarmOnOff = (SwitchButton) view
					.findViewById(R.id.item_alarm_switch);
			holder.alarmTag = (TextView) view.findViewById(R.id.item_alarm_tag);
			holder.remainTime = (AlarmRemainTimeTextView) view
					.findViewById(R.id.item_alarm_reamin_time);
			holder.alarmTitle = (TextView) view
					.findViewById(R.id.item_alarm_title);
			holder.alarmTime = (AndroidClockTextView) view
					.findViewById(R.id.item_alarm_time);
			holder.alarmCheckBox = (CheckBox) view
					.findViewById(R.id.item_alarm_checkbox);
		} else {
			holder = (ViewHolder) view.getTag();
		}
		holder.remainTime.setAlarmOn(alarm.enabled);
		holder.remainTime.setDestTime(AlarmHelper.calculateAlarmTime(alarm));
		holder.alarmTag.setText(alarm.daysOfWeek.toString(mContext, true));
		holder.alarmTitle.setText(alarm.label);
		String timeStr = String.format(Locale.getDefault(), "%02d:%02d",
				alarm.hour, alarm.minutes);
		holder.alarmTime.setText(timeStr);
		if (mListView.getChoiceMode() == ListView.CHOICE_MODE_NONE) {
			holder.alarmCheckBox.setVisibility(View.GONE);
			holder.alarmOnOff.setVisibility(View.VISIBLE);
			holder.alarmOnOff.setChecked(alarm.enabled);
			holder.alarmOnOff.setTag(alarm);
			holder.alarmOnOff.setOnMyCheckedChangeListener(this);
		} else {
			holder.alarmOnOff.setVisibility(View.GONE);
			holder.alarmOnOff.setOnMyCheckedChangeListener(null);
			holder.alarmCheckBox.setVisibility(View.VISIBLE);
			boolean checked = mListView.isItemChecked(cursor.getPosition());
			holder.alarmCheckBox.setChecked(checked);
		}
	}

	@Override
	public void onCheckedChanged(CompoundButton buttonView, boolean isChecked,
			boolean fromUser) {
		if (fromUser && buttonView.getTag() != null
				&& buttonView.getTag() instanceof Alarm) {
			Alarm alarm = (Alarm) buttonView.getTag();
			if (isChecked != alarm.enabled) {
				Log.d(TAG, "onCheckedChanged()--->alarm[" + alarm.id
						+ "] enable:" + alarm.enabled + ",now turn:"
						+ isChecked);
				AlarmHelper.enableAlarm(mContext, alarm.id, isChecked);
				if (isChecked) {
					SetAlarmActivity.popAlarmSetToast(mContext, AlarmHelper
							.getAlarm(mContext.getContentResolver(), alarm.id));
				}
			}
		}
	}

	private class ViewHolder {
		public SwitchButton alarmOnOff;
		public TextView alarmTitle;
		public TextView alarmTag;
		public AlarmRemainTimeTextView remainTime;
		public AndroidClockTextView alarmTime;
		public CheckBox alarmCheckBox;
	}

}
