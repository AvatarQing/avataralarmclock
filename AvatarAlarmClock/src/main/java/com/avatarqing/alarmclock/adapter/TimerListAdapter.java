package com.avatarqing.alarmclock.adapter;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.database.Cursor;
import android.support.v4.widget.CursorAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ListView;

import com.avatarqing.alarmclock.activity.SetTimerActivity;
import com.avatarqing.alarmclock.entity.Timer;
import com.avatarqing.alarmclock.util.TimerHelper;
import com.avatarqing.alarmclock.widget.SwitchButton;
import com.avatarqing.alarmclock.widget.TimerRemainTimeTextView;
import com.avatarqing.alarmclock.widget.SwitchButton.OnMyCheckedChangeListener;
import com.bounique.tools.alarmclock.R;

public class TimerListAdapter extends CursorAdapter implements
		OnMyCheckedChangeListener {
	private static final String TAG = TimerListAdapter.class.getSimpleName();
	private Context mContext = null;
	private ListView mListView = null;

	public TimerListAdapter(Context context, ListView listview, Cursor cursor,
	                        int flag) {
		super(context, cursor, flag);
		mContext = context;
		mListView = listview;
	}

	@Override
	public View newView(Context context, Cursor cursor, ViewGroup parent) {
		View rootView = LayoutInflater.from(context).inflate(
				R.layout.item_timer, parent, false);
		return rootView;
	}

	@Override
	public void bindView(View view, Context context, Cursor cursor) {
		final Timer timer = new Timer(cursor);
		ViewHolder holder = null;
		if (view.getTag() == null) {
			holder = new ViewHolder();
			holder.remainTime = (TimerRemainTimeTextView) view
					.findViewById(R.id.item_timer_reamin_time);
			holder.timerOnOff = (SwitchButton) view
					.findViewById(R.id.item_timer_switch);
			holder.checkBox = (CheckBox) view
					.findViewById(R.id.item_timer_checkbox);
		} else {
			holder = (ViewHolder) view.getTag();
		}
		holder.remainTime.setTimerOn(timer.enabled);
		holder.remainTime.setTimeArgs(timer.startTime, timer.hour,
				timer.minutes, timer.second);
		if (mListView.getChoiceMode() == ListView.CHOICE_MODE_NONE) {
			holder.checkBox.setVisibility(View.GONE);
			holder.timerOnOff.setVisibility(View.VISIBLE);
			holder.timerOnOff.setChecked(timer.enabled);
			holder.timerOnOff.setTag(timer);
			holder.timerOnOff.setOnMyCheckedChangeListener(this);
		} else {
			holder.timerOnOff.setVisibility(View.GONE);
			holder.timerOnOff.setOnMyCheckedChangeListener(null);
			holder.checkBox.setVisibility(View.VISIBLE);
			boolean checked = mListView.isItemChecked(cursor.getPosition());
			holder.checkBox.setChecked(checked);
		}
	}

	@Override
	public void onCheckedChanged(final CompoundButton buttonView,
	                             boolean isChecked, boolean fromUser) {
		if (fromUser && buttonView.getTag() != null
				&& buttonView.getTag() instanceof Timer) {
			final Timer timer = (Timer) buttonView.getTag();
			if (isChecked != timer.enabled) {
				if (isChecked) {
					SetTimerActivity.popTimerSetToast(mContext, TimerHelper
							.getTimer(mContext.getContentResolver(), timer.id));
				} else {
					// 选择了关闭
					// 提示用户是否定时器，不删除就不给关闭
					new AlertDialog.Builder(mContext)
							.setTitle(mContext.getString(R.string.delete_timer))
							.setMessage(
									mContext.getString(R.string.delete_timer_confirm))
							.setPositiveButton(android.R.string.ok,
									new DialogInterface.OnClickListener() {
										public void onClick(DialogInterface d,
										                    int w) {
											// 删除定时器
											TimerHelper.deleteTimer(mContext,
													timer.id);
										}
									})
							.setNegativeButton(android.R.string.cancel,
									new DialogInterface.OnClickListener() {
										public void onClick(DialogInterface d,
										                    int w) {
											// 禁止
											buttonView.setChecked(true);
										}
									}).show();
				}
			}
		}
	}

	private class ViewHolder {
		public SwitchButton timerOnOff;
		public TimerRemainTimeTextView remainTime;
		public CheckBox checkBox;
	}
}